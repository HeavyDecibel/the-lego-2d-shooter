#pragma once

#include "GameEngine.h"
#include "Sprite.h"

class Player;
class Waypoint;

enum ShotType{
	SHOT,
	QUADSHOT,
	HOMINGMISSILE,
	SLOWAIMEDSHOT
};

class Shot
{

protected:
	DirectX* directX;
	Sprite* sprite;
	bool active;
	ShotType shotType;
	Waypoint* target;
	bool destructable;
	bool exploding;
	float speed;

public:
	Shot(DirectX* directX);
	virtual ~Shot();

	Shot();

	Sprite* getSprite();
	virtual void drawSprite(bool animation);
	bool isActive();
	void toggleStatus();
	virtual void moveShot(float distance);
	string getType();
	ShotType getShotType();
	void setTarget(Player* player);
	void setTarget(float x, float y);
	virtual void explodeAnimation(bool animation);
	bool isExploding();
	void explode();
	bool isDestructable();

};

