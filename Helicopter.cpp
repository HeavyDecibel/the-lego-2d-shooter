/*

	En helikopterfienden som best�r av tv� sprites, en f�r kroppen och en f�r 
	de roterande rotorbladen. Den sista n�mnda roteras ett visst antal grader i varje iteration.
	Fienden skjuter quadshots

 */


#include "Level.h"
#include "Helicopter.h"
#include "Shot.h"
#include "Waypoint.h"
#include "Timer.h"
#include "QuadShot.h"

Helicopter::Helicopter(DirectX* directX, float xPos, float yPos, float speed) :Enemy(directX, xPos, yPos, speed)
{

	//Skapar upp de tv� sprites samt en lista med waypoints och en timer som best�mmer hur ofta fienden anfaller

	sprite = new Sprite(14, 64, 0, 0, xPos, yPos, 1, 1, 1, directX, "Graphics/Sprites/Helicopter.png");
	rotor = new Sprite(14, 60, 0, 0, xPos, yPos, 1, 1, 1, directX, "Graphics/Sprites/Rotor.png");
	rotorRotation = 0;

	waypoint.push_back(new Waypoint(xPos, 200));
	waypoint.push_back(new Waypoint(xPos - 250, 500));
	waypoint.push_back(new Waypoint(xPos + 250, 700));
	waypoint.push_back(new Waypoint(xPos, 1200));

	attackTimer->setNewDelay(500);

}


Helicopter::~Helicopter()
{

	//Tar bort rotorn. �vrigt tas bort med basklassens destruktor.

	delete rotor;

}


void Helicopter::move(){

	//Fienden r�r sig precis som en vanlig fiende, men rotorbladen flytas �ven i samband med att kroppen g�r det.

	Enemy::move();
	rotor->setNewPos(xPos + 1, yPos + 2);
}

void Helicopter::draw(){

	//Ritar ut spriten samt roterar rotorbladen

	rotorRotation -= 40;
	if (rotorRotation < 0)
		rotorRotation = 360;

	rotor->rotateSprite(rotorRotation);
	sprite->drawSprite();
	rotor->drawSprite();

}

void Helicopter::attackV3(Level* level){
	
	//Anfaller med fyra stycken quadshots.

	if (attackTimer->done()){
		for (int j = 0; j < 4; j++){
			for (int k = 0; k < level->getENEMYSHOTS(); k++){ //Letar upp l�gsta inaktiva missilnumret
				if (!level->getEnemyShots(k)->isActive() && level->getEnemyShots(k)->getShotType()==QUADSHOT){
					level->getEnemyShots(k)->toggleStatus();
					level->getEnemyShots(k)->getSprite()->setNewPos(this->getSprite()->getX(), this->getSprite()->getY());
					break;
				}
			}
		}
	}
}
