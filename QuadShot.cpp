/*
	Ett skott som r�r sig i en av fyra riktningar fr�n avfyraren.
	Om man fyrar av fyra stycken samtidigt s� ser det ut som om fienden skickar iv�g fyra skott i olika riktningar
*/


#include "QuadShot.h"

int QuadShot::directionCounter = 0;

QuadShot::QuadShot(DirectX* directX):Shot(directX)
{
	
	//Kontrollerar vilken riktning skottet ska ha n�r det genereras.

	if (directionCounter > 3)
		directionCounter = 0;

	switch (directionCounter){
		case 0:
			direction = topleft;
			break;
		case 1:
			direction = topright;
			break;
		case 2:
			direction = bottomleft;
			break;
		case 3:
			direction = bottomright;
			break;
	}
	
	directionCounter++;
	shotType=QUADSHOT;

}

QuadShot::~QuadShot()
{

}

void QuadShot::moveShot(float distance){

	//Flyttar skottet i den riktningen som �r satt.

	switch (direction){
		
		case topleft:
			sprite->setNewPos(sprite->getX() + 8.0f, sprite->getY() - 8.0f);
			break;

		case topright:
			sprite->setNewPos(sprite->getX() + 8.0f, sprite->getY() + 8.0f);
			break;

		case bottomleft:
			sprite->setNewPos(sprite->getX() - 8.0f, sprite->getY() - 8.0f);
			break;

		case bottomright:
			sprite->setNewPos(sprite->getX() - 8.0f, sprite->getY() + 8.0f);
			break;
	}

}