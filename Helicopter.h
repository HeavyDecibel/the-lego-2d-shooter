#pragma once

#include "Enemy.h"

class Helicopter :
	public Enemy
{

private:
	Sprite* rotor;
	int rotorRotation;


public:
	Helicopter(DirectX* directX, float xPos, float yPos, float speed);
	~Helicopter();

	virtual void move() override;
	virtual void draw() override;
	virtual void attackV3(Level* level) override;
};
