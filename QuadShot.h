#pragma once
#include "Shot.h"

enum Direction{

	topleft,
	topright,
	bottomleft,
	bottomright

};

class QuadShot :
	public Shot
{

	Direction direction;
	static int directionCounter;

public:
	QuadShot(DirectX* directX);
	~QuadShot();

	virtual void moveShot(float distance);

};

