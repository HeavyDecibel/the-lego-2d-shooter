/*

	Wrapperklass f�r ljud

*/


#include "DirectSound.h"
#include <string>


DirectSound::DirectSound(string filename, HWND window) :
filename(filename)
{

	//Konstruktor som skapar upp en ny soundmanager

	soundManager = new CSoundManager();
	soundManager->Initialize(window, DSSCL_PRIORITY);
	soundManager->SetPrimaryBufferFormat(2, 22050, 16);
	soundManager->Create(&sound, const_cast<char*>(this->filename.c_str()));
	paused = false;

}


DirectSound::~DirectSound()
{
	delete sound;
	delete soundManager;
}

void DirectSound::play(){

	//Ljud spelas upp

	sound->Play();
	paused = false;

}

void DirectSound::play(bool loop){

	//Ljud spelas upp med eller utan loop (bra f�r t ex musik)
	
	if (loop)
		sound->Play(0, DSBPLAY_LOOPING);
	else
		sound->Play();

}

void DirectSound::reset(){

	//Ljud nollst�lls

	sound->Reset();

}

void DirectSound::stop(){

	//Ljud stoppas

	sound->Stop();

}

void DirectSound::pause(){

	//Och pausas

	sound->Stop();
	paused = true;

}

bool DirectSound::isPaused(){

	//returerar om ljudet �r i pausl�ge eller inte

	return paused;

}