#pragma once
#include "Enemy.h"
class StuntFlyer :
	public Enemy
{


public:
	StuntFlyer(DirectX* directX, float xPos, float yPos, float speed);
	~StuntFlyer();

	virtual void attackV3(Level* level);
};

