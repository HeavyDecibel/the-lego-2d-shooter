#pragma once

#include "Enemy.h"

class JetPlane :
	public Enemy
{
private:

public:
	JetPlane(DirectX* directX, float xPos, float yPos, float speed);
	~JetPlane();

};
