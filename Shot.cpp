/*

Basklass f�r skottobjekten. M�ls�kande skott har en waypoint som m�l som de r�r sig mot.
En del skott g�r dessutom att f�rst�ra om man skjuter p� dom, d� anv�nds flaggan f�r destructable

*/

#include "Shot.h"
#include "Player.h"
#include "Waypoint.h"


Shot::Shot(){

}

Shot::Shot(DirectX* directX)
{
	target = new Waypoint(0, 0);
	sprite = new Sprite(11, 11, 0, 0, 0, 0, 6, 1, 6, directX, "Graphics/Sprites/Shots.png");
	active = false;
	destructable = false;

}


Shot::~Shot()
{

	delete sprite;
	delete target;
}

void Shot::drawSprite(bool animation){

	//Om skottet �r aktivt s� ritas det ut och animeras om s� �nskas.

	if (active){
		
		if (animation)
			sprite->animateSprite();
		
		sprite->drawSprite();
	}
		
}

bool Shot::isActive(){
	
	//Returnerar om skottet �r aktivt eller inte
	
	return active;
}

void Shot::toggleStatus(){

	//S�tter skottet till aktivt om det �r inaktivt och vice versa.
	//Det h�r l�t smart fr�n b�rjan f�r d� slapp jag skriva tv� metoder
	//men jag har m�rkt att det finns tillf�llen d� det kr�nglade till det, s� 
	//l�xan �r l�rd till en annan g�ng.

	if (active)
		active = false;
	else
		active = true;

}

Sprite* Shot::getSprite(){

	//Returnerar referens till spriten

	return sprite;

}

void Shot::moveShot(float distance){

	//Flyttar skottet i valfri hastighet

	int newYPos = sprite->getY();
	newYPos += distance;
	sprite->setNewPos(sprite->getX(), newYPos);

}

ShotType Shot::getShotType(){
	
	//Returnerar vilken typ av skott det �r som i sin tur styrs av en enum.

	return shotType;
}

void Shot::setTarget(Player *player){

	//Tar spelarens sprite och g�r dess nuvarande position till m�lpunkt

	target->x = player->getSprite()->getX();
	target->y = player->getSprite()->getY();

}

void Shot::setTarget(float x, float y){

	//�verladdad version av ovanst�ende, fast den tar koordinater ist�llet.

	target->x = x;
	target->y = y;

}

void Shot::explodeAnimation(bool animation){
	//Animeringarna sker i de klasser som kan exploderas.
}

bool Shot::isDestructable(){
	
	//Returnerar om skottet g�r att f�rst�ra eller ej
	
	return destructable;
}

void Shot::explode(){
	
	//Initierar en explosionanimering
	
	exploding = true;
}

bool Shot::isExploding(){
	
	//Returnerar om skottet h�ller p� att explodera eller ej.
	
	return exploding;
}