#pragma once
#include "Shot.h"

#define PI 3.14159265

class HomingMissile :
	public Shot
{

private:

	Sprite *explosionSprite;
	

public:
	HomingMissile(DirectX *directX);
	~HomingMissile();

	virtual void moveShot(float distance) override;
	virtual void drawSprite(bool animation) override;
	virtual void explodeAnimation(bool animation) override;
};

