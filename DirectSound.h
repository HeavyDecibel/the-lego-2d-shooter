#pragma once

#include "GameEngine.h"
#include "DSUtil.h"

class DirectSound
{
private:

	string filename;
	CSoundManager* soundManager;
	CSound* sound;
	bool paused;

public:

	DirectSound(string filename, HWND window);
	~DirectSound();

	void play();
	void play(bool loop);
	void pause();
	void stop();
	void reset();
	bool isPaused();

};

