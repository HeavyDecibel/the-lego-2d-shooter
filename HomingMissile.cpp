/*
	
	M�ls�kande missil som alltid r�r sig mot fienden
	Den h�r g�r �ven att f�rsta av spelarens skott

*/

#include "HomingMissile.h"
#include "Waypoint.h"
#include "Player.h"


HomingMissile::HomingMissile(DirectX *directX) :Shot(directX)
{

	delete sprite;
	sprite = new Sprite(30, 120, 0, 0, 0, 0, 1, 1, 1, directX, "Graphics/Sprites/HomingMissile.png");
	explosionSprite = new Sprite(16, 16, 0, 0, 0, 0, 16, 4, 4, directX, "Graphics/Explosions/SmallExplosion.png");

	destructable = true;
	shotType = HOMINGMISSILE;
	speed = 3.0f;
	exploding = false;

}


HomingMissile::~HomingMissile()
{
	delete explosionSprite;
}

void HomingMissile::moveShot(float distance){

	
	//Flyttar skottet mot spelaren, men den roterar �ven spriten s� att nosen alltid pekar
	//i den riktningen som den ska �ka

	//Rotera spriten
	float deltaY = sprite->getY() - target->y;
	float deltaX = sprite->getX() - target->x;

	int angleInDegrees = atan2(deltaY, deltaX) * 180 / PI;

	sprite->rotateSprite(angleInDegrees + 90);
	
	
	if (sprite->getX() < target->x)
		sprite->setNewPos(sprite->getX() + speed, sprite->getY());
	if (sprite->getX() > target->x)
		sprite->setNewPos(sprite->getX() - speed, sprite->getY());

	if (sprite->getY() < target->y)
		sprite->setNewPos(sprite->getX(), sprite->getY() + speed);
	if (sprite->getY() > target->y)
		sprite->setNewPos(sprite->getX(), sprite->getY() - speed);

}

void HomingMissile::drawSprite(bool animation){

	//Ritar ut spriten

	if (active){
		sprite->drawSprite();
	}

}

void HomingMissile::explodeAnimation(bool animate){

	//Vid tr�ff s� exloderar spriten

	explosionSprite->setNewPos(sprite->getX(), sprite->getY());
	
	if (animate)
		explosionSprite->animateSprite();
	explosionSprite->drawSpriteV2();
	if (explosionSprite->getFrame() == explosionSprite->getNoFrames())
		exploding = false;

}