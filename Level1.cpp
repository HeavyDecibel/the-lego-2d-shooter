#include "Level1.h"
#include "Background.h"
#include "JetPlane.h"
#include "Helicopter.h"
#include "StuntFlyer.h"
#include "SkyPirate.h"
#include "QuadShot.h"
#include "SlowAimedShot.h"
#include "HomingMissile.h"
#include "Timer.h"

Level1::Level1(DirectX *directX, Player* player):Level(directX, player)
{

	//Varje level har en vector med fiender (som tar typen Enemy) som fylls p� lite godtyckligt.

	for (int i = 0; i < 28; i++){
		
		if (i <= 9){
			enemyVector.push_back(new JetPlane(directX, -200, 300, 6.0f));
		}
		
		if (i >= 10 && i <= 11){
			
			float startPos = rand() % ((screenWidht - 150) - 0) + 0;
			enemyVector.push_back(new StuntFlyer(directX, startPos, 0, 4.0f));
		}
		
		if (i>=12 && i <= 21){
			enemyVector.push_back(new JetPlane(directX, -200, 300, 6.0f));

		}

		
		if (i >= 22 && i<=25){
			float heloStartPos = rand() % ((screenWidht - 150) - 0) + 0;
			enemyVector.push_back(new Helicopter(directX, heloStartPos, 0, 5.0f));

		}

		if (i == 26){
			enemyVector.push_back(new SkyPirate(directX, 900, 1200, 4.0f));
		}
		
	}

	//Samt en Vector med shots som tar olika typer av skott.
	//De h�r f�rbest�ms och �teranv�nds senare under spelet

	for (int i = 0; i < 65; i++){
		if (i <= 40){
			shotVector.push_back(new QuadShot(directX));
		}

		if ( i>=41 && i <=60){
			shotVector.push_back(new SlowAimedShot(directX));
		}

		if (i >= 61 && i <= 64){
			shotVector.push_back(new HomingMissile(directX));
		}
	}

	//De h�r tv� variablerna la jag till f�r att se om prestandan �kade n�got
	//om jag slapp att r�kna ut storleken i varje iteration av spelloopen, men jag har inte m�rkt n�gon skillnand
	noOfEnemies = enemyVector.size();
	noOfShots = shotVector.size();

	//H�r finns �ven en timmer som styr med vilken mellanrum fienderna skickas ut p� sk�rmen
	spawnTimer->setNewDelay(5000);

	//Och en bakgrund
	background = new Background(directX, "Level1", 3.0f, 255);
	
}


Level1::~Level1()
{

}


void Level1::updateSpawnTimer(){

	//Metod som styr n�r de olika fienderna ska genereras p� sk�rmen
	//Tiden �ndras beroende p� hur m�nga som skickats ut p� sk�rmen f�r
	//att f� variation i spelet.

	switch (spawnedEnemies){
		case 1:
			spawnTimer->setNewDelay(500);
			break;
		case 10:
			spawnTimer->setNewDelay(5000);
			break;
		case 11:
			spawnTimer->setNewDelay(2000);
			break;
		case 12:
			spawnTimer->setNewDelay(5000);
			break;
		case 13:
			spawnTimer->setNewDelay(500);
			break;
		case 22:
			spawnTimer->setNewDelay(5000);
			break;
		case 23:
			spawnTimer->setNewDelay(1500);
			break;
	}
}

