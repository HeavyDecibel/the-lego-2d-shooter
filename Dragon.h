#pragma once
#include "Enemy.h"
class Dragon :
	public Enemy
{
public:
	Dragon(DirectX* directX, float xPos, float yPos, float speed);
	~Dragon();

	virtual void move() override;

};

