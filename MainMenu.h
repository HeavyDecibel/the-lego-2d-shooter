#pragma once

#include "GameEngine.h"

class DirectX;
class MenuButton;
class MouseCursor;
class Sprite;

class MainMenu
{
private:
	DirectX *directX;
	MenuButton* newGameButton;
	MenuButton* resumeGameButton;
	MenuButton* saveGameButton;
	MenuButton* loadButton;
	MenuButton* exitGameButton;
	MouseCursor* mouseCursor;
	Sprite* menuFrame;
	Sprite* menuFrameBackground;

	void destroyMenu();

public:
	MainMenu(DirectX *directX);
	~MainMenu();

	void drawMenu(GameStatus gameStatus);
	void drawMouseCursor(float x, float y);
	Sprite* getNewGameButton();
	Sprite* getResumeGameButton();
	Sprite* getSaveButton();
	Sprite* getLoadButton();
	Sprite* getExitGameButton();
	Sprite* getMouseCursor();

};

