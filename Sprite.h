//Basklass f�r sprites, det ska f�r framtida projekt g� att �rva fr�n den h�r. Men den ska inneh�lla allting som kr�vs
//f�r att kunna rita ut och flytta en sprite. 

#pragma once

#ifndef __SPRITE_H_INCLUDED__
#define __SPRITE_H_INCLUDED__

#include "GameEngine.h"
#include "DirectX.h"
#include <string>

class Timer;

class Sprite
{
private:
	LPD3DXSPRITE sprite; //<-spriteobjeket
	LPDIRECT3DTEXTURE9 texture; //<-texturen

	RECT map; //Rektangel f�r var i bildfilen spriten finns
	int mapLeft, mapTop; //x, y-koordinater i bildfilen
	int width, height; //Spritens storlek
	int frame; //aktuell ruta i animationen
	int noFrames; //<-Antalet animationer per sprite
	float scaling; //<-skalningsniv�n
	float rotate; //<-rotationsgraden. 0=center, detta �r i grader och beh�ver r�knas om till radaner
	std::string filename; //bildens s�kv�g
	int transparency; //<-Niv�n p� genomskinlighet
	float x, y; //x, y-koordinater p� sk�rmen
	Timer* animationTimer;
	DirectX* directX;

	int columns, rows; //F�r 2D-spritesheets. OBS!!!!! <--G�R OM DET H�R F�R ALLA SPRITES SEDAN.

	int direction; //F�r diagonalskott. G�r om till enum eller liknande sedan (eller hitta en b�ttre l�sning)

	void createSprite(std::string filename);
	void createTimer();

public:

	//Konstruktor och destruktor (destrukorn �r tom, s� den kunde lika g�rna vara default-destruktorn)
	Sprite();
	Sprite(int width, int heigt, int mapLeft, int mapTop, float x, float y, int noFrames, int rows, int cols, DirectX* directX, string filename);
	~Sprite();

	void drawSprite();//<-Ritar ut spriten p� sk�rmen
	void recreateSprite(); //<-Skapar om spriten
	void setNewPos(float x, float y); //<-S�tter en ny position
	void rotateSprite(float degree); //<-Roterar spriten
	void animateSprite();
	void setTransparencyLevel(int transparency); //<-S�tter genomskinlighetsgrad
	void scaleSprite(float scale); //<-S�tter skala
	void moveSpriteX(float movement);
	void moveSpriteY(float movement);
	void moveLeft(float movement);
	void moveRight(float movement);
	void moveUp(float movement);
	void moveDown(float movement);
	float getX();
	float getY();
	int getWidth();
	int getHeight();
	void setFrame(int frame);
	int getFrame();
	int getNoFrames();
	void setTimer(int time);

	void setDirection(int direction);
	int getDirection();

	Sprite* nextElement;

	//*************F�R TEST**********************
	void drawSpriteV2();

};

#endif



