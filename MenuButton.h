#pragma once
#include <string>
#include "DirectX.h"
#include "Sprite.h"

class MenuButton
{
private:

	Sprite* buttonSprite;
	string buttonText;
	DirectX* directX;
	float xPos, yPos;

	void deleteButton();

public:
	
	MenuButton(string text, DirectX* directX, float xPos, float yPos);
	~MenuButton();

	void drawButton();
	Sprite* getSprite();

};

