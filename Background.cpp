/*
Klass som styr spelets scrollande bakgrund
H�r finns metoder f�r inl�sning fr�n fil samt utritande av sprites och
s�klart �ven scrollmekanism. H�r finns, liksom i levelklassen ett p�b�rjat f�rs�k till 
serilisering. 

*/

#include "Background.h"
#include "Sprite.h"

Background::Background(DirectX *directX, string directory, float speed, byte transparency):
directX(directX),
directory(directory),
scrollspeed(speed),
transparency(transparency)
{
	
	//Konstruktorn som skapar upp bakgrunden
	//F�rutom n�dv�ndigheter dom directX s� tar den emot i vilken hastighet
	//bakgrunden ska scrollas, i vilken mapp som filerna f�r spritesheeten och v�rdena f�r tilemapen finn
	//samt vilken transparens som sprites ska ha. Det sista �r n�dv�ndigt f�r att molnen ska vara en smula genomskinliga.

	this->directory = "Levels/" + directory;
	string path = this->directory + ".png"; //S�kv�g till bildfilen
	
	readLevelFromFile();

	sprite = new Sprite(120, 120, 0, 0, 0, 0, 7, 0, 0, directX, path);
	sprite->setTransparencyLevel(transparency);
	setUpSprites();
	
}


Background::~Background()
{
	
	//Rensar bort structen som styr var spriten ska ritas ut
	for (int cols = 0; cols < MAPHEIGHT; cols++){
		for (int rows = 0; rows < MAPWIDTH; rows++){
			delete spriteInfo[rows][cols];
		}
	}

	//Och tar bort spriten
	delete sprite;
}

void Background::readLevelFromFile(){

	//Metod som l�ser in tilemapen fr�n en textfil

	string path = directory + ".txt";
	ifstream file(path);

	char nextChar;
	int cols = 0;
	int rows=0;

	if (!file.is_open())
		MessageBox(NULL, "Mapfile not found.", "File not found error.", MB_OK);

	
	if (file.is_open()){
		
		while (file.get(nextChar)){
			if (nextChar > 47 && nextChar < 58){
				tileMap[rows][cols] = nextChar - '0'; //G�r om teckent fr�n Ascii till vanliga siffror;
				rows++;
			}
			if (nextChar == '\n'){
				rows = 0;
				cols++;
			}
		}
		file.close();
	}
}


void Background::setUpSprites(){

	//Fyller tilemapen med v�rden f�r var spriten ska ritas ut

	float xPos, yPos;

	for (int cols = 0; cols < MAPHEIGHT; cols++){
		for (int rows = 0; rows < MAPWIDTH; rows++){

			xPos = rows * 120;
			yPos = (cols * 120) - 120;

			spriteInfo[rows][cols] = new SpritePos(xPos, yPos, tileMap[rows][cols]);

		}
	}
}


void Background::scrollBackground(){
	
	//Funktion som scrollar bakgrunden i den hastighet som angett i konstruktorn 

	float xPos, yPos;

	for (int cols = 0; cols < MAPHEIGHT; cols++){
		for (int rows = 0; rows < MAPWIDTH; rows++){

			if (spriteInfo[rows][cols]->y >= (GAMEWORLDHEIGHT)-(120) - (120)){
				spriteInfo[rows][cols]->y = -120;
				sprite->setNewPos(spriteInfo[rows][cols]->x, spriteInfo[rows][cols]->y);
			}
				
			spriteInfo[rows][cols]->y += scrollspeed;
		}
	}
	
}

void Background::drawBackground(){

	//Ritar ut bakgrunden p� sk�rmen. Samma sprites ritas ut flera g�nger efter vad som
	//finns i structen SpriteInfo

	int counter = 0;

	for (int cols = 0; cols < MAPHEIGHT; cols++){
		for (int rows = 0; rows < MAPWIDTH; rows++){
			sprite->setNewPos(spriteInfo[rows][cols]->x, spriteInfo[rows][cols]->y);
			sprite->setFrame(spriteInfo[rows][cols]->frame);
			sprite->drawSprite();
			counter++;
		} 
	}
}

#pragma region Serialisation

void Background::load(string file){

	ifstream saveFile;
	saveFile.open(file);
	char nextChar;
	int rowNo = 1;
	string input = "";

	while (saveFile.get(nextChar)){
		if (nextChar == '\n')
			rowNo++;

		if (rowNo == 2){
			if (nextChar != '\n'){
				input += nextChar;
			}
		}

		if (rowNo > 2)
			break;
	}
	saveFile.close();
	scrollPos = atoi(input.c_str());

}

void Background::save(string file, int level){

	ofstream saveFile;
	saveFile.open(file, ios::app);

	string backgroundSaveFileRow = "";
	backgroundSaveFileRow += to_string(scrollPos) + "\n";
	
	saveFile << backgroundSaveFileRow.c_str();

	saveFile.close();

}

#pragma endregion