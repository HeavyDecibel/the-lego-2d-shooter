/*

	Menyn som anv�nds som del huvudmeny och dels som menyn under spelets g�ng.

*/

#include "MainMenu.h"
#include "DirectX.h"
#include "MenuButton.h"
#include "MouseCursor.h"
#include "Sprite.h"


MainMenu::MainMenu(DirectX *directX):
directX(directX)
{

	//Skapar upp knappar, muspekare och ram

	newGameButton = new MenuButton("New game", directX, 605, 300);
	resumeGameButton = new MenuButton("  Resume", directX, 1005, 300);
	saveGameButton = new MenuButton("    Save", directX, 1005, 500);
	loadButton = new MenuButton("    Load", directX, 605, 500);
	exitGameButton = new MenuButton("Exit game", directX, 1005, 700);
		
	mouseCursor = new MouseCursor(directX);
	menuFrame = new Sprite(1300, 784, 0, 0, 310, 148, 1, 1, 1, directX, "Graphics/GUI/MenuFrame.png");
	menuFrameBackground = new Sprite(1189, 658, 0, 0, 360, 210, 1, 1, 1, directX, "Graphics/GUI/MenuFrameBackground.png");
	menuFrameBackground->setTransparencyLevel(220);

}


MainMenu::~MainMenu()
{

	destroyMenu();

}

void MainMenu::destroyMenu(){

	delete newGameButton;
	delete resumeGameButton;
	delete loadButton;
	delete saveGameButton;
	delete exitGameButton;
	delete mouseCursor;
	delete menuFrameBackground;
	delete menuFrame;

}

void MainMenu::drawMenu(GameStatus gameStatus){

	//Ritar upp menyn p� sk�rmen. Om spelet k�rs s� visas �ven Resume-knappen

	menuFrameBackground->drawSpriteV2();
	menuFrame->drawSpriteV2();
	
	newGameButton->drawButton();
	if (gameStatus==INGAMEMENU)
		resumeGameButton->drawButton();
	saveGameButton->drawButton();
	loadButton->drawButton();
	exitGameButton->drawButton();

}

void MainMenu::drawMouseCursor(float x, float y){

	//Ritar ut muspekaren
	
	mouseCursor->drawCursor(x, y);

}

//Returnerar referenser till knapparna s� att GameEngine kan hantera dom

Sprite* MainMenu::getNewGameButton(){
	return newGameButton->getSprite();
}


Sprite* MainMenu::getMouseCursor(){
	return mouseCursor->getSprite();
}

Sprite* MainMenu::getResumeGameButton(){
	return resumeGameButton->getSprite();
}

Sprite* MainMenu::getSaveButton(){
	return saveGameButton->getSprite();
}


Sprite* MainMenu::getLoadButton(){
	return loadButton->getSprite();
}

Sprite* MainMenu::getExitGameButton(){
	return exitGameButton->getSprite();
}
