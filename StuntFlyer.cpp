#include "StuntFlyer.h"
#include "Level.h"
#include "Shot.h"
#include "SlowAimedShot.h"
#include "Sprite.h"
#include "Waypoint.h"
#include "Timer.h"
#include "Player.h"


StuntFlyer::StuntFlyer(DirectX* directX, float xPos, float yPos, float speed) :Enemy(directX, xPos, yPos, speed)
{

	sprite = new Sprite(61, 61, 0, 0, xPos, yPos, 1, 1, 1, directX, "Graphics/Sprites/StuntFlyer.png");

	waypoint.push_back(new Waypoint(xPos, 200));
	waypoint.push_back(new Waypoint(xPos, 500));
	waypoint.push_back(new Waypoint(xPos, 1200));
	

	attackTimer->setNewDelay(750);
	int a = 5;
}


StuntFlyer::~StuntFlyer()
{
}


void StuntFlyer::attackV3(Level* level){

	if (attackTimer->done()){
		for (int k = 0; k < level->getENEMYSHOTS(); k++){ //Letar upp l�gsta inaktiva missilnumret
			if (!level->getEnemyShots(k)->isActive() && level->getEnemyShots(k)->getShotType() == SLOWAIMEDSHOT){
				level->getEnemyShots(k)->toggleStatus();
				level->getEnemyShots(k)->getSprite()->setNewPos(this->getSprite()->getX(), this->getSprite()->getY());
				//R�knar ut skottets riktning. 
				float x=0.0f;

				if (level->getPlayer()->getSprite()->getX()>sprite->getX())
					x = (float)screenWidht + 25;
				else
					x = -25.0f;
				
				float y = level->getPlayer()->getSprite()->getY();
				level->getEnemyShots(k)->setTarget(x, y);
				break;
			}
		}
	}

}