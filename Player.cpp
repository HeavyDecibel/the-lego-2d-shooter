/*

Den h�r klassen hanterar spelaren sprite
Spelaren kan flytta p� sig i alla riktning samt explodera
N�r spelaren d�r s� blir han/hon os�rbar i ett par sekunder f�r 
att inte genast d� direkt, f�r att markera detta blinkar spelarens
sprite i ett par sekunder.

*/

#include "Player.h"
#include "Sprite.h"
#include "Timer.h"

Player::Player(DirectX* directX):
directX(directX) {

	//Konstruktorn s�tter upp spelaren och dess sprites, samt antal liv etc fr�n b�rjan
	//H�r finns �ven tv� timers, en som styr hur l�nge spelaren �r od�dlig och en som styr hur l�ng tid 
	//det ska ta mellan varja blinkning f�r spriten.

	name = "Player name"; //H�r var det t�nkt att man skulle f� mat in namn f�r t ex highscore, men det f�r bli till senare ut�kning

	invincible = false;
	flashing = false;
	invinvibilityTimer = new Timer(2500);
	flashTimer = new Timer(50);
	
	sprite = new Sprite(120, 138, 0, 0, 0, 0, 1, 1, 1, directX, "Graphics/Sprites/X-wing.png");
	explosionSprite = new Sprite(114, 114, 0, 0, 0, 0, 15, 3, 5, directX, "Graphics/Explosions/PlayerExplosion.png");
	explosionSprite->setTimer(50);
	resetPlayer(); 

}

Player::~Player(){

	//Tar bort alla pekare som �r skapade med New

	delete sprite;
	delete explosionSprite;
	delete invinvibilityTimer;
	delete flashTimer;

}

void Player::drawSprite(){

	//Ritar ut spriten

	sprite->drawSprite();

}

short Player::getLives(){

	//Returnerar antal liv

	return lives;

}

short Player::getHealth(){

	//Returnerar spelarens h�lsa. Spelaren t�l sex tr�ffar innan denne d�r.

	return health;

}

void Player::reduceHealth(){

	//R�knar av h�lsan ett steg
	
	health--;

}

void Player::reduceLife(){

	//Tar bort ett av spelarens liv

	lives--;

}

void Player::movePlayerX(float movement){

	//Flyttar splearen i X-ledd

	float newXPos = sprite->getX();
	newXPos += movement;

	sprite->setNewPos(newXPos, sprite->getY());

}
void Player::movePlayerY(float movement){

	//Flyttar spelaren i Y-ledd

	float newYPos = sprite->getY();
	newYPos += movement;

	sprite->setNewPos(sprite->getX(), newYPos);

}

Sprite* Player::getSprite(){

	//Returnerar en referens av spriten

	return sprite;

}

void Player::checkDeath(){

	//Kontrollerar om spelaren fortfarande lever och om den exploderar eller �r od�dlig

	if (lives > 0){
		if (health < 1 && !exploding){
			lives--;
			exploding = true;
		}


		if (invinvibilityTimer->done())
			invincible = false;

		//Om spelaren �teruppst�r s� blinkar spelarens sprite f�r att indikera od�dlighet
		//Det sk�ts med en timer som s�tter p� alt. st�nger av transparensens p� spriten

		if (invincible){
			if (flashTimer->done()){
				if (!flashing){
					sprite->setTransparencyLevel(0);
					flashing = true;
				}
				else{
					sprite->setTransparencyLevel(255);
					flashing = false;
				}
			}
		}
		else{
			sprite->setTransparencyLevel(255);
		}
	}

}

void Player::explode(bool animate){
	
	//Animerar spelarens explosion. Om spelaren har exploderat klart s� g�rs den klar f�r spel igen.

	invincible = true;
	explosionSprite->setNewPos(sprite->getX(), sprite->getY());
	
	if (animate)
		explosionSprite->animateSprite();
	explosionSprite->drawSpriteV2();
	if (explosionSprite->getFrame() == explosionSprite->getNoFrames()){ 
		if (lives > 0){
			restorePlayer();
			invinvibilityTimer->resetTimer();
			flashTimer->resetTimer();
			invincible = true;
		}
		explosionSprite->setFrame(0);
		exploding = false;
	}
}

bool Player::isExploding(){
	
	//Returnerar om spelaren exploderar eller inte

	return exploding;
}

void Player::restorePlayer(){
	
	//�terst�ller spelaren s� den kan forts�tta spelet efter att ha d�tt
	
	exploding = false;
	health = 6;
	recenterPlayer();
}

void Player::recenterPlayer(){
	
	//S�tter spelarspriten i mitten av sk�rmen

	sprite->setNewPos((screenWidht / 2) - (sprite->getWidth() / 2), (screenHeight / 2) - (sprite->getHeight() / 2));
}

void Player::resetPlayer(){

	//Nollst�ller spelaren i ursprungsl�ge

	restorePlayer();
	lives = 3;
	score = 0;
}

bool Player::isInvincible(){
	
	//Returnerar om spelaren �r od�dlig eller ej
	
	return invincible;
}

#pragma region Serialistion

void Player::load(string file){

	//Informationen om spelaren finns alltid p� rad 1 i den sparade filen.
	//Vi l�ser allts� in texten som finns p� den raden och n�r vi kommer till en radbrytning s� bryts
	//inl�sningen och sedan f�rs v�rdena �ver till Player-objektet.
	
	ifstream saveFile;
	saveFile.open(file);
	char nextChar;
	string chars[7]; //Sju positioner att l�sa in. Namn, score, lives, health, bannummer, x- och y-pos.
	int pos = 0;

	while (saveFile.get(nextChar)){
		
		if (nextChar == '\n')
			break;

		if(nextChar != ',' && nextChar != '\n'){
			chars[pos] += nextChar;
		}

		if (nextChar == ','){
			pos++;
			chars[pos] = "";
		}
	}
	
	saveFile.close();

	name = chars[0];
	score = atoi(chars[1].c_str());
	lives = atoi(chars[2].c_str());
	health = atoi(chars[3].c_str());
	savedLevel = atoi(chars[4].c_str());
	sprite->setNewPos(atoi(chars[5].c_str()), atoi(chars[6].c_str()));
	
}

void Player::save(string file, int level){
	
	//Sparar ner alla varibler f�r spelaren till en kommaseparerad rad i en textfil.

	savedLevel = level;

	ofstream saveFile;
	saveFile.open(file);

	string playerSaveFileRow = "";
	playerSaveFileRow += name + ",";
	playerSaveFileRow += to_string(score) + ",";
	playerSaveFileRow += to_string(lives) + ",";
	playerSaveFileRow += to_string(health) + ",";
	playerSaveFileRow += to_string(savedLevel) + ",";
	playerSaveFileRow += to_string(sprite->getX()) + ",";
	playerSaveFileRow += to_string(sprite->getY()) + "\n";

	saveFile << playerSaveFileRow.c_str();
	
	saveFile.close();

}

int Player::atLevel(){
	
	//Anv�nds f�r att GameEngine ska veta vilken niv� den ska ladda in.
	
	return savedLevel;
}


#pragma endregion

#pragma region Oanv�nda metoderf�r framtiden

void Player::addScore(int score){

	//L�gger till po�ng

	this->score += score;

}

string Player::getName(){

	//Returnerar spelarens namn, �r som i po�ngens fall inte implementerat

	return name;

}

void Player::setName(string name){

	//S�tter spelarens namn

	this->name = name;

}

int Player::getScore(){

	//Returnerar po�ngen. Jag har inte implemnterat po�ngr�kning den h�r g�ngen
	//men har planer p� att bygga ut spelet fram�ver och d� kommer det beh�vas.

	return score;

}

#pragma endregion
