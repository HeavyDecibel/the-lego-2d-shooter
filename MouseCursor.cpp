/*
	Klass som ritar ut muspekaren
*/


#include "MouseCursor.h"


MouseCursor::MouseCursor(DirectX* directX):
directX(directX)
{

	cursorSprite = new Sprite(25, 26, 1, 1, 1, 1, 1, 1, 1, directX, "Graphics/GUI/MouseCursor.png");

}

MouseCursor::~MouseCursor()
{

	deleteMouseCursor();

}

void MouseCursor::deleteMouseCursor(){

	delete cursorSprite;

}

void MouseCursor::drawCursor(float xPos, float yPos){

	//Tar emot muspekarens position och ritar ut den
	
	cursorSprite->setNewPos(xPos, yPos);
	cursorSprite->drawSprite();

}

float MouseCursor::getXPos(){
	return cursorSprite->getX();
}

float MouseCursor::getYPos(){
	return cursorSprite->getY();
}

Sprite* MouseCursor::getSprite(){
	return cursorSprite;
}