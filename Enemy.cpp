/*

Basklass f�r fiender. Den sk�ter r�relser och har abstracta metoder f�r underklasserna
Det skapas aldrig upp en instans av den h�r klassen utan bara av underklasserna.

*/


#include "Enemy.h"
#include "DirectX.h"
#include "Sprite.h"
#include "Waypoint.h"
#include "Timer.h"

Enemy::Enemy(DirectX* directX, float xPos, float yPos, float speed)
{

	//Konstruktorn som st�ller in grundv�derna, tar en referens av directX-objektet etc.
	//s�tter st�ller �ven in en defaultexplosion. N�gra av de andra klasserna byter ut den h�r 
	//sedan i sina konstruktorer.

	this->directX = directX;
	this->xPos = xPos;
	this->yPos = yPos;
	this->speed = speed;

	currentWaypoint = 0;
	active = false;
	destroyed = false;

	explosionSprite = new Sprite(128, 128, 0, 0, 0, 0, 64, 8, 8, directX, "Graphics/Explosions/StandardExplosion.png");
	explosionSprite->setTimer(1); //Timer f�r animation

	attackTimer = new Timer(500); //Timer f�r hur ofta fienden anfaller

	lastWaypointReached = false; //Om fienden n�tt sista m�lpunkten s� inaktiveras den.

	strenght = 1; //Hur mycket stryk fienden t�l
}


Enemy::~Enemy()
{

	//Tar bort sprites, timers, waypoints etc.

	delete sprite;
	delete explosionSprite;
	delete attackTimer;

	for (int i = 0; i < waypoint.size(); i++)
		delete waypoint[i];

	waypoint.clear();
	waypoint.shrink_to_fit();

}

void Enemy::move(){
	
	//Metod f�r att flytta fienden. Det finns en lista med m�lpunkter som g�s igenom
	//och s� l�nge fienden �r aktiv s� r�r den sig mot aktuell m�lpunkt.
	//N�r den n�tt den s� g�r den till n�sta punkt i vectorn och vid sista punkten s� inaktiveras fienden.
	//H�r finns �ven en funktion som roterar fienden s� att nosen alltid pekar mot m�lpunkten.

	if (active && !lastWaypointReached){
		if (!waypoint[currentWaypoint]->reached){

			float deltaY = yPos - waypoint[currentWaypoint]->y;
			float deltaX = xPos - waypoint[currentWaypoint]->x;

			int angleInDegrees = atan2(deltaY, deltaX) * 180 / PI; //R�knar ut vinkel mot m�let
			sprite->rotateSprite(angleInDegrees + 90); //Rotation mot m�lpunkten.

			if (xPos < waypoint[currentWaypoint]->x)
				xPos += speed;
			if (xPos > waypoint[currentWaypoint]->x)
				xPos -= speed;
			if (yPos < waypoint[currentWaypoint]->y)
				yPos += speed;
			if (yPos > waypoint[currentWaypoint]->y)
				yPos -= speed;

			if (xPos<waypoint[currentWaypoint]->x + speed && xPos>waypoint[currentWaypoint]->x - speed){
				if (yPos<waypoint[currentWaypoint]->y + speed && yPos>waypoint[currentWaypoint]->y - speed){
					waypoint[currentWaypoint]->reached = true;
					if (currentWaypoint < waypoint.size()-1)
						currentWaypoint++;
					else{
						active = false;
						lastWaypointReached = true;
					}

				}
			}
		}
	}

	//Uppdaterar postionen f�r spriten samt f�r explosionsspriten.
	sprite->setNewPos(xPos, yPos);
	explosionSprite->setNewPos(xPos - 20, yPos - 20);
}


void Enemy::drawObject(){
	
	//Ritar ut spriten
	
	sprite->drawSprite();
}


//Returnerar X och Y
float Enemy::getX(){

	return xPos;

}

float Enemy::getY(){

	return yPos;

}

void Enemy::setPosition(float xPos, float yPos){

	//St�ller in positionen av spriten

	this->xPos = xPos;
	this->yPos = yPos;

}

bool Enemy::isActive(){
	
	//Returnerar om fienden �r aktiv eller inte
	
	return active;
}

void Enemy::setInactive(){

	//G�r fienden inaktiv

	active = false;

}

Sprite* Enemy::getSprite(){

	//Returnerar en referens till spriten

	return sprite;

}

void Enemy::setDestroyd(){
	
	//S�tter fineden till f�rst�rd
	
	destroyed = true;
}

bool Enemy::isDestroyed(){
	
	//Returnerar om fienden �r f�rst�rd eller inte
	
	return destroyed;
}

void Enemy::explode(bool animate){

	//Exploderar fienden med eller utan animation

	if (animate)
		explosionSprite->animateSprite();
	explosionSprite->drawSpriteV2();
	if (explosionSprite->getFrame() == explosionSprite->getNoFrames())
		exploded = true;

}

bool Enemy::getExploded(){
	
	//Returnerar om fienden �r f�rst�rd eller inte
	
	return exploded;
}

void Enemy::draw(){
	
	//Ritar ut fiendenspriten
	
	sprite->drawSprite();
}

void Enemy::attackV3(Level* level){
	//Metod f�r fiendes attack. Alla fiender anfaller olika s� detta sk�ts i respektive underklass.
}


void Enemy::setActive(){
	
	//S�tter fienden till aktiv
	
	active = true;
}

bool Enemy::getLastWaypointReached(){
	
	//Returnerar om fienden n�tt sin sista m�lpunkt i listan
	
	return lastWaypointReached;
}


int Enemy::getStrenght(){
	
	//Returerar hur mycket styrka fienden har kvar
	
	return strenght;
}

void Enemy::reduceStrenght(){
	
	//Reducerar styrka med 1
	
	strenght--;
}