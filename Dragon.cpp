/*
	Den h�r fienden �r ganska lik JetPlane, den anfaller inte utan f�ljer bara sina m�lpunkter.
	D�remot �r den mycket starkare och kan animeras
*/

#include "Dragon.h"
#include "Sprite.h"
#include "Waypoint.h"


Dragon::Dragon(DirectX* directX, float xPos, float yPos, float speed) :Enemy(directX, xPos, yPos, speed)
{

	//St�ller in grundv�rden och lista med m�lpunkter

	waypoint.push_back(new Waypoint(xPos, 200));
	waypoint.push_back(new Waypoint(xPos, 500));
	waypoint.push_back(new Waypoint(xPos, 1200));
	
	sprite = new Sprite(400, 334, 0, 0, xPos, yPos, 1, 1, 2, directX, "Graphics/Sprites/Dragon.png");
	sprite->setTimer(250);

	delete explosionSprite;
	explosionSprite = new Sprite(350, 350, 0, 0, 0, 0, 15, 3, 5, directX, "graphics/Explosions/DragonExplosion.png");

	strenght = 25;
	

}

Dragon::~Dragon()
{
}

void Dragon::move(){
	
	//R�r sig precis som basklassen, men animeras �ven d� den har tv� frames.
	
	sprite->animateSprite();
	Enemy::move();
}


