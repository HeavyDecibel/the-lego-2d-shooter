/*
	Klass som ritar upp knapparna med en text
*/


#include "MenuButton.h"


MenuButton::MenuButton(string text, DirectX* directX, float xPos, float yPos):
buttonText(text),
directX(directX),
xPos(xPos),
yPos(yPos)
{

	buttonSprite = new Sprite(250, 100, 0, 0, xPos, yPos, 1, 1, 1, directX, "Graphics/GUI/MenuButton.png");

}

void MenuButton::deleteButton(){

	delete buttonSprite;
	
}

MenuButton::~MenuButton()
{
	
	deleteButton();

}

void MenuButton::drawButton(){

	buttonSprite->drawSprite();
	directX->printText(buttonText.c_str(), xPos + (20), yPos + (50 / 2), D3DCOLOR_XRGB(224, 255, 255));

}

Sprite* MenuButton::getSprite(){
	return buttonSprite;
}
