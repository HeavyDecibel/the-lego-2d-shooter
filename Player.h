#pragma once

#include <string>
#include "GameEngine.h"
#include "ISerializable.h"

class Sprite;
class Timer;

using namespace std;

class Player: ISerializable {

private:
	
	string name;
	unsigned int score;
	short lives;
	short health;
	Sprite* sprite;
	Sprite* explosionSprite;
	DirectX* directX;
	bool exploding;
	bool invincible;
	Timer* invinvibilityTimer;
	Timer* flashTimer;
	bool flashing;
	int savedLevel;
	
	void restorePlayer();


public:

	Player(DirectX* directX);
	~Player();

	void drawSprite();
	short getLives();
	short getHealth();
	int getScore();
	void reduceHealth();
	void reduceLife();
	string getName();
	void setName(string name);
	void movePlayerX(float movement);
	void movePlayerY(float movement);
	Sprite* getSprite();
	void addScore(int score);
	void checkDeath();
	void explode(bool animate);
	bool isExploding();
	bool isInvincible();
	void resetPlayer();
	void recenterPlayer();
	int atLevel();

	virtual void save(string file, int level) override;
	virtual void load(string file) override;

};