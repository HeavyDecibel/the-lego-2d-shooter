#pragma once

#include <string>
#include <fstream>
#include <windows.h> //f�r Byte.
#include "ISerializable.h"

//Forward declarations
class DirectX;
class Sprite;

using namespace std;

class Background:ISerializable
{
	
	struct SpritePos{

		int x, y, frame;

		SpritePos(int spriteX, int spriteY, int spriteFrame){
			x = spriteX;
			y = spriteY;
			frame = spriteFrame;
		}
	};
	
	DirectX *directX;
	float scrollspeed;
	byte transparency;
	Sprite* sprite;
	
	int scrollPos;
	string directory;
	
	static const int MAPWIDTH = 16;
	static const int MAPHEIGHT = 36;

	SpritePos *spriteInfo[MAPWIDTH][MAPHEIGHT];
	int tileMap[MAPWIDTH][MAPHEIGHT];

	const int GAMEWORLDWIDTH = 120*MAPWIDTH;
	const int GAMEWORLDHEIGHT = 120*MAPHEIGHT;

	void readLevelFromFile();
	void setUpSprites();


public:
	Background(DirectX *directX, string directory, float speed, byte transparency);
	
	~Background();

	void scrollBackground();
	void drawBackground();

	virtual void load(string file) override;
	virtual void save(string file, int level = -1) override;

};

