#include "Level3.h"
#include "Background.h"
#include "Helicopter.h"
#include "QuadShot.h"
#include "Timer.h"


Level3::Level3(DirectX *directX, Player* player) :Level(directX, player)
{
	background = new Background(directX, "Level3", 3.0f, 255);
	spawnTimer->setNewDelay(2500);
	
	for (int i = 0; i < 10; i++){
		float heloStartPos = rand() % ((screenWidht - 150) - 0) + 0;
		enemyVector.push_back(new Helicopter(directX, heloStartPos, 0, 5.0f));
	}
	
	for (int i = 0; i < 25; i++){
		shotVector.push_back(new QuadShot(directX));
	}
	
	noOfEnemies = enemyVector.size();
	noOfShots = shotVector.size();

}


Level3::~Level3()
{
}

void Level3::updateSpawnTimer(){
	if (spawnedEnemies == 1)
		spawnTimer->setNewDelay(1500);
}
