#pragma once
#include "Enemy.h"
class SkyPirate :
	public Enemy
{
private:

	Timer *secondaryAttackTimer;

public:
	SkyPirate(DirectX* directX, float xPos, float yPos, float speed);
	~SkyPirate();

	virtual void move() override;
	virtual void attackV3(Level* level) override;
};

