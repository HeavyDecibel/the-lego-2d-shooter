#pragma once
#include "Shot.h"

class Waypoint;
class Player;

class SlowAimedShot :
	public Shot
{

public:
	SlowAimedShot(DirectX *directX);
	~SlowAimedShot();

	virtual void moveShot(float distance) override;

};

