#pragma once

#include "DirectX.h"
#include "Sprite.h"

class MouseCursor
{
private:
	Sprite* cursorSprite;

	DirectX* directX;
	float xPos, yPos;

	void deleteMouseCursor();

public:
	MouseCursor(DirectX* directX);
	~MouseCursor();

	void drawCursor(float xPos, float yPos);
	float getXPos();
	float getYPos();
	Sprite* getSprite();
};

