#pragma once

#include <math.h>
#include <vector>

#define PI 3.14159265

class Level;
class Sprite;
class Waypoint;
class Shot;
class DirectX;
class Timer;

using namespace std;

class Enemy
{
protected:
	float xPos, yPos;
	float speed;
	DirectX* directX;
	Sprite* sprite;
	Sprite* explosionSprite;
	int currentWaypoint;
	bool active;
	bool lastWaypointReached;
	bool destroyed;
	bool exploded;
	Level* level;
	vector<Waypoint*> waypoint;
	Timer* attackTimer;
	int strenght;


public:
	Enemy(DirectX* directX, float xPos, float yPos, float speed);
	virtual ~Enemy();

	virtual void drawObject();
	void setPosition(float xPos, float yPos);
	float getX();
	float getY();
	virtual void move();
	virtual void draw();
	bool isActive();
	void setInactive();
	void setActive();
	void setDestroyd();
	Sprite* getSprite();
	bool isDestroyed();
	void explode(bool animate);
	bool getExploded();
	virtual void attackV3(Level* level);
	bool getLastWaypointReached();
	void reduceStrenght();
	int getStrenght();
};


