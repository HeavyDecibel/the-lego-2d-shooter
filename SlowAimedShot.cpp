/*
 Ett skott som riktas mot spelarens position
 */


#include "SlowAimedShot.h"
#include "Waypoint.h"
#include "Player.h"


SlowAimedShot::SlowAimedShot(DirectX *directX) :Shot(directX)
{

	shotType = SLOWAIMEDSHOT;

}


SlowAimedShot::~SlowAimedShot()
{

}

void SlowAimedShot::moveShot(float distance){

	//R�kna ut vinkeln mot punkten och l�t den r�ra sig �t det h�llet

	if (sprite->getX() < target->x)
		sprite->setNewPos(sprite->getX() + distance, sprite->getY());
	if (sprite->getX() > target->x)
		sprite->setNewPos(sprite->getX() - distance, sprite->getY());

	if (sprite->getY() < target->y)
		sprite->setNewPos(sprite->getX(), sprite->getY() + distance);
	if (sprite->getY() > target->y)
		sprite->setNewPos(sprite->getX(), sprite->getY() - distance);

}