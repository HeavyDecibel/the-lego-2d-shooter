/*
Huvudkodfilen som styr hela spelet
H�rifr�n styrs input fr�n spelaren, menyutskick, grafikutritning,
kollisionshantering, ljuduppspelning och liknande
*/

#include <vector>
#include "DirectInput.h"
#include "Sprite.h"
#include "DirectSound.h"
#include "MainMenu.h"
#include "HUD.h"
#include "Player.h"
#include "Shot.h"
#include "Enemy.h"
#include "Level.h"
#include "Level1.h"
#include "Level2.h"
#include "Level3.h"
#include "Timer.h"

#include "psapi.h" //F�r minnestest

//Grundl�ggande parameterar f�r om spelet �r ig�ng eller ej
//samt sk�rmstorlek. 
bool gameRunning = true;
int screenWidht = 1920;
int screenHeight = 1080;

//DirectX-objekt
DirectX *directX;
DirectInput *input;

//Enum som styr vilket l�ge spelet �r i
GameStatus gameStatus;

//Ljud, bilder, menyer etc
Sprite *splashScreen;
Sprite *loadScreen;
MainMenu *mainMenu;
HUD *hudPanel;
Player *player;

DirectSound *fire;
DirectSound *explosion;

//Parametrar f�r spelarens skott. Max 30 stycken kan ritas ut samtidigt.
static const int PLAYERSHOTS = 30;
vector<Shot*> shots;

//Inl�st niv� samt hur l�ngt spelaren har kommit
int stage;
vector<Level*> level;

//Timer f�r gameover-skylten
Timer *gameOverTimer;

bool backgroundOn = true; //F�r testsyfte enbart


//**********************TEST F�R MINNESBER�KNING*******************
DWORDLONG totalVirtualMem;
DWORDLONG virtualMemUsed;
DWORDLONG totalPhysMem;
SIZE_T physMemUsedByMe;
SIZE_T virtualMemUsedByMe;
//**********************TEST F�R MINNESBER�KNING*******************

void clearAllLevels(){

	//Rensar alla banor s� att bara EN �r inl�st i minnet. 
	//Loopar �nd� igenom vectorn, f�r man vet ju aldrig...

	for (int i = 0; i < level.size(); i++)
		delete level[i];

	level.clear();

}

void resetGame(){

	//Nollst�ller spelet och laddar in f�rsta banan
	clearAllLevels();
	level.push_back(new Level1(directX, player));
	for (int i = 0; i < shots.size(); i++)
		shots[i]->toggleStatus();
	
	player->resetPlayer();
	
	gameStatus = RUNNING;
	stage = 1;

}

void setUpNewLevel(){
	
	//N�r man klarat en bana s� skapas n�sta bana upp
	//alternativt s� har man klarat spelet och d� skickas man till huvudmenyn
	clearAllLevels();
	gameStatus = RUNNING;
	player->recenterPlayer();

	switch (stage){
		
		case 2:
			level.push_back(new Level2(directX, player));
			break;
		case 3:
			level.push_back(new Level3(directX, player));
			break;
		default:
			gameStatus = MAINMENU;
			break;
	}
}

void setUpGameObjects(){

	//S�tter uppobjekten vid start av programmet. 
	//H�r skapas en spleare upp, splashscreen, laddsk�rm, meny samt gr�nsnittet som anv�nds under spelets g�ng.
	//�ven spelaren skott laddas in h�r och status s�tts s� att spelet hamnar i Splashscreenl�ge.

	player = new Player(directX);

	splashScreen = new Sprite(1920, 1080, 0, 0, 0, 0, 1, 1, 1, directX, "Graphics/Backgrounds/SplashScreen.png");
	loadScreen = new Sprite(1920, 1080, 0, 0, 0, 0, 1, 1, 1, directX, "Graphics/Backgrounds/LoadScreen.png");
	mainMenu = new MainMenu(directX);
	hudPanel = new HUD(directX, player);
	
	gameOverTimer = new Timer(5000); //Tiden som GameOver-texten visas om man skulle r�ka ut f�r det

	for (int i = 0; i < PLAYERSHOTS; i++){
		shots.push_back(new Shot(directX));
	}

	gameStatus = SPLASHSCREEN; 

}

#pragma region MINNESTEST

//*****************************************F�R TEST!!!!****************************

void calculateMemoryUsage(){

	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);
	totalVirtualMem = memInfo.ullTotalPageFile;

	virtualMemUsed = memInfo.ullTotalPageFile - memInfo.ullAvailPageFile;

	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(GetCurrentProcess(), (PROCESS_MEMORY_COUNTERS*)&pmc, sizeof(pmc));
	SIZE_T virtualMemUsedByMe = pmc.PrivateUsage;

	physMemUsedByMe = 0;
	totalPhysMem = memInfo.ullTotalPhys;
	physMemUsedByMe = pmc.WorkingSetSize;


}

#pragma endregion

//*****************************************F�R TEST!!!!****************************

bool initGame(HWND window){
	
	//H�r laddas de grundl�ggande funkterna in som t ex DirectX och DirectInput
	//Om n�got g�r fel h�r s� returneras falskt och loopen i WinMain avbryts.

	srand((unsigned int)time(NULL)); //Skapar ett nytt slumpfr�
	
	directX = new DirectX(window);
	if (!directX)
		return false;
	
	input = new DirectInput(window);
	if (!input)
		return false;

	stage = 1;

	setUpGameObjects();

	fire = new DirectSound("Sounds\\Shot.wav", window);
	explosion = new DirectSound("Sounds\\Explosion.wav", window);

	ShowCursor(false);

	return true;

}

bool detectBoxCollision(Sprite* spriteA, Sprite* spriteB){

	//Boxkollision, funktionen tar tv� sprites och r�knar ut en rektangel runt de tv�
	//och om de �verlappar s� returneras en krock

	if (spriteA->getX()>spriteB->getX() &&
		(spriteA->getX() + spriteA->getWidth())<(spriteB->getX() + spriteB->getWidth()) &&
		spriteA->getY()>spriteB->getY() &&
		(spriteA->getY() + spriteA->getHeight()) < (spriteB->getY() + spriteB->getHeight()))
		return true;

	return false;

}

bool detectCollision(Sprite* spriteA, Sprite* spriteB){

	//Funktion f�r cirkel-cirkel-kollisionshantering s� som den anv�nds i kurslitteraturn
	//Den r�knar ut avst�ndet mellan tv� mittpunkter (som i sin tur �r beroende p� hur 
	//stora spritsen �r som skickas in i funktionen). Om avst�ndet �r mindre �n det tv� radierna s� 
	//r�knas det som krock

	double radious1, radious2;

	//Radie f�r objekt 1:

	if (spriteA->getWidth() > spriteA->getHeight())
		radious1 = spriteA->getWidth() / 2.0;
	else
		radious1 = spriteA->getHeight() / 2.0;

	//Mittpunkt f�r objekt 1
	double x1 = spriteA->getX() + radious1;
	double y1 = spriteA->getY() + radious1;
	D3DXVECTOR2 vector1((float)x1, (float)y1);

	//Radie f�r objekt 2:
	if (spriteB->getWidth() > spriteB->getHeight())
		radious2 = spriteB->getWidth() / 2.0;
	else
		radious2 = spriteB->getHeight() / 2.0;

	//Mittpunkt f�r objekt 2:
	double x2 = spriteB->getX() + radious2;
	double y2 = spriteB->getY() + radious2;
	D3DXVECTOR2 vector2((float)x2, (float)y2);

	double deltax = vector1.x - vector2.x;
	double deltay = vector1.y - vector2.y;
	double distance = sqrt((deltax*deltax) + (deltay*deltay));

	return (distance < radious1 + radious2);

}

void checkCollisions(){

	//Lite l�tt f�renklat s� g�r varje loop igenom aktiva skott, fiender samt spelaren och kontrollerar
	//om de krockar med varandra. Fienderna kan aldrig krocka med varandra eller sina egna skott.
	//Vid krock s� s�tts fiendeobjekt och skott till inaktiva och eventuellt f�rst�rda om kraften tar slut

	//Spelarens skott vs fiender och fiendes skott
	for (int i = 0; i < PLAYERSHOTS; i++){
		if (shots[i]->isActive()){
			for (int j = 0; j < level[0]->getENEMIES(); j++){
				if (level[0]->getEnemies(j)->isActive()){
					if (detectCollision(shots[i]->getSprite(), level[0]->getEnemies(j)->getSprite())){
						shots[i]->toggleStatus();
						level[0]->getEnemies(j)->reduceStrenght();
						if (level[0]->getEnemies(j)->getStrenght() < 1){
							level[0]->getEnemies(j)->setInactive();
							level[0]->getEnemies(j)->setDestroyd();
							explosion->reset();
							explosion->play();
						}
					}
				}
			}
			for (int j = 0; j < level[0]->getENEMYSHOTS(); j++){
				if (level[0]->getEnemyShots(j)->isDestructable() && level[0]->getEnemyShots(j)->isActive()){
					if (detectCollision(shots[i]->getSprite(), level[0]->getEnemyShots(j)->getSprite()))
						level[0]->getEnemyShots(j)->toggleStatus();
					//L�gg ev. till en liten explosion h�r sedan
				}
			}
		}
	}

	if (!player->isInvincible()){ //S� l�nge spelaren inte �r od�dlig (vilket h�nder precis efter att han/hon d�tt.
		//Spelare vs fiender
		for (int i = 0; i < level[0]->getENEMIES(); i++){
			if (level[0]->getEnemies(i)->isActive() && !level[0]->getEnemies(i)->isDestroyed()){
				if (detectCollision(player->getSprite(), level[0]->getEnemies(i)->getSprite())){
					player->reduceHealth();
					level[0]->getEnemies(i)->reduceStrenght();
					if (level[0]->getEnemies(i)->getStrenght() < 1){
						level[0]->getEnemies(i)->setDestroyd();
						level[0]->getEnemies(i)->setInactive();
					}
				}
			}
		}

		//Fiendens skott vs spelare
		for (int i = 0; i < level[0]->getENEMYSHOTS(); i++){
			if (level[0]->getEnemyShots(i)->isActive()){
				if (detectCollision(level[0]->getEnemyShots(i)->getSprite(), player->getSprite())){
					level[0]->getEnemyShots(i)->toggleStatus();
					level[0]->getEnemyShots(i)->explode();
					player->reduceHealth();
				}
			}
		}
	}
}

bool outOfBounds(Sprite* sprite){

	//Kollar om skott/fiender �r utanf�r sk�rmen.
	if (sprite->getY() < 0 - sprite->getHeight() || sprite->getY() > screenHeight + sprite->getHeight() || sprite->getX() < 0 - sprite->getWidth() || sprite->getY() > screenWidht + sprite->getWidth())
		return true;

	return false;

}

void checkPlayerOutOfBounds(){
	//Kollar s� att spelaren inte �ker utanf�r sk�rmen

	//Kolla botten
	if (player->getSprite()->getY() > 800)
		player->movePlayerY(-6.0f);

	//Kolla toppen
	if (player->getSprite()->getY() < 0)
		player->movePlayerY(6.0f);
	
	//Kolla h�ger
	if (player->getSprite()->getX() > screenWidht - player->getSprite()->getWidth())
		player->movePlayerX(-6.0f);

	//Kolla v�nster
	if (player->getSprite()->getX() < 0)
		player->movePlayerX(6.0f);
	
}

void checkGameOver(){
	
	//Kontrollerar vilkor f�r spelets avslut.
	//Om spelarens liv �r nere i 0 s� avslutas spelet efter att gameovertimern k�rts klart
	if (gameStatus != GAMEOVER){
		if (player->getLives() < 1){
			gameStatus = GAMEOVER;
			gameOverTimer->resetTimer();
		}
	}

	//N�r timern k�rts klart s� skickas spelaren till huvudmenyn
	if (gameStatus == GAMEOVER){
		if (gameOverTimer->done())
			gameStatus = MAINMENU;
	}
}

void fireShot(){

	//Skjuter iv�g n�sta aktiva skott i vectorn �ver spelarens skott.
	
	for (int i = 0; i < PLAYERSHOTS; i++){ //Letar upp l�gsta inaktiva missilnumret
		if (!shots[i]->isActive()){
			shots[i]->toggleStatus();
			shots[i]->getSprite()->setNewPos(player->getSprite()->getX(), player->getSprite()->getY());
			break;
		}
	}


	for (int i = 0; i < PLAYERSHOTS; i++){ //Letar upp l�gsta inaktiva missilnumret
		if (!shots[i]->isActive()){
			shots[i]->toggleStatus();
			shots[i]->getSprite()->setNewPos(player->getSprite()->getX() + 110, player->getSprite()->getY());
			break;
		}
	}

	//Och spelar upp ett litet ljud.
	fire->reset();
	fire->play();

}

void spawnEnemies(){

	//Genererar upp n�sta aktiva fiende efter den listan som finns i varje levelklass
	//H�r finns lite regler som att fienden inte f�r vara f�rst�rd eller n�tt sin sista waypoint (vilket i teorin �ven ska g�ra den inaktiv
	//men better safe than sorry).

	if (level[0]->getSpawnTimer()->done()){
		for (int i = level[0]->getSpawnedEnemies(); i < level[0]->getENEMIES(); i++){
			if (!level[0]->getEnemies(i)->isActive() && !level[0]->getEnemies(i)->isDestroyed() && !level[0]->getEnemies(i)->getLastWaypointReached()){
				level[0]->getEnemies(i)->setActive();
				level[0]->increaseSpawnedEnemies();
				break;
			}
		}
	}
	level[0]->updateSpawnTimer();
}

void moveShots(){

	//Flyttar spelerens aktiva skott mot sk�rmens topp.

	for (int i = 0; i < PLAYERSHOTS; i++){
		if (shots[i]->isActive()){
			shots[i]->moveShot(-8.0f); //Byt ut det h�r mot ett fast v�rde satt i skottklassen
			if (shots[i]->getSprite()->getY() < 0)
				shots[i]->toggleStatus();
		}

	}

	//Flyttar fiendens aktiva skott beroende p� vad de �r f�r typ.
	//Beroende lite p� hur skottens moveShot-funktion ser ut s� r�r de sig lite olika.
	for (int i = 0; i < level[0]->getENEMYSHOTS(); i++){
		if (level[0]->getEnemyShots(i)->isActive()){
			level[0]->getEnemyShots(i)->moveShot(8.0f);

			if (outOfBounds(level[0]->getEnemyShots(i)->getSprite())){
				level[0]->getEnemyShots(i)->toggleStatus();
			}
		}
	}

}

void moveSprites(){

	//Flyttar fiendens sprite. Det h�r sk�ts i respektive fiendeklass.
	for (int i = 0; i < level[0]->getENEMIES(); i++){
		if (level[0]->getEnemies(i)->isActive())
			level[0]->getEnemies(i)->move();
	}

}

void enemyAttackV2(){

	//Om fienden kan attackera s� g�rs det h�r. Vilken typ av attack som utf�rs sker i fiendenklassen
	//men det �r alltid n�gon form av skott som genereras.

	for (int i = 0; i < level[0]->getENEMIES(); i++){
		if (level[0]->getEnemies(i)->isActive() && !level[0]->getEnemies(i)->isDestroyed()){
			level[0]->getEnemies(i)->attackV3(level[0]);
		}
	}
}

void renderGraphics(HWND window){

	//H�r ritas all grafik ut

	directX->clearScreen((0, 0, 0));

	directX->getDevice()->BeginScene();
	{
		//H�r styrs allting av i vilket l�ge spelet befinner sig i, om det �r pausat om in-gamemenyn syns etc
		
		if (gameStatus != SPLASHSCREEN){
			if (gameStatus != LOADSCREEN){
				if (gameStatus == RUNNING || gameStatus == PAUSED || gameStatus == INGAMEMENU || gameStatus==GAMEOVER){

					//Bakgrund ritas ut med eller utan scroll
					if (backgroundOn){
						if (gameStatus == RUNNING || gameStatus==GAMEOVER)
							level[0]->scrollBackground(true);
						else
							level[0]->scrollBackground(false);
					}

					//Spelaren ritas ut i explosionsl�ge med eller utan animation
					if (player->isExploding()){
						if (gameStatus == RUNNING || gameStatus == GAMEOVER)
							player->explode(true);
						else
							player->explode(false);
					}
					else{
						//Den normala spelarspriten ritas ut
						if (gameStatus!=GAMEOVER)
							player->drawSprite();
					}

					//Spelarens skott ritas ut
					for (int i = 0; i < PLAYERSHOTS; i++){
						if (gameStatus == RUNNING || gameStatus == GAMEOVER)
							shots[i]->drawSprite(true);
						else
							shots[i]->drawSprite(false);
					}

					//Fiendernas skott ritas ut
					for (int i = 0; i < level[0]->getENEMYSHOTS(); i++){
						if (!level[0]->getEnemyShots(i)->isExploding()){
							if (gameStatus == RUNNING || gameStatus == GAMEOVER)
								level[0]->getEnemyShots(i)->drawSprite(true);
							else
								level[0]->getEnemyShots(i)->drawSprite(false);
						}
						else{
							if (gameStatus == RUNNING || gameStatus == GAMEOVER)
								level[0]->getEnemyShots(i)->explodeAnimation(true);
							else
								level[0]->getEnemyShots(i)->explodeAnimation(false);
						}
						
					}

					//Fienderna ritas ut
					for (int i = 0; i < level[0]->getENEMIES(); i++){
						if (gameStatus == RUNNING || gameStatus == PAUSED || gameStatus == INGAMEMENU || gameStatus==GAMEOVER){ 
							if (level[0]->getEnemies(i)->isActive())
								level[0]->getEnemies(i)->draw();
							if (level[0]->getEnemies(i)->isDestroyed()){
								if (!level[0]->getEnemies(i)->getExploded()){
									if (gameStatus == RUNNING || gameStatus == GAMEOVER)
										level[0]->getEnemies(i)->explode(true);
									else
										level[0]->getEnemies(i)->explode(false);
								}
							}
						}
					}
					//Anv�ndargr�nsnittet ritas ut
					hudPanel->drawHUD();
				}

				//Blinkande gameover-text skrivs ut
				if (gameStatus == GAMEOVER){
					string outPutText = "Game Over!";

					byte r = rand() % 256;
					byte g = rand() % 256;
					byte b = rand() % 256;

					D3DCOLOR fontColor = D3DCOLOR_XRGB(r, g, b);

					directX->printText(outPutText.c_str(), 800, 500, fontColor);
				}

				//Huvudmenyn ritas ut med olika bakgrund beroende p� l�ge
				if (gameStatus == MAINMENU || gameStatus == INGAMEMENU){

					if (gameStatus != INGAMEMENU)
						splashScreen->drawSpriteV2();

					mainMenu->drawMenu(gameStatus);
					mainMenu->drawMouseCursor(input->mouseX(), input->mouseY());
				}
			}
			else{
				//Laddsk�rmen...
				loadScreen->drawSprite();
			}
		}
		else{
			//...och splashsk�rmen ritas ut.
			splashScreen->drawSprite();
		}
	}

	directX->getDevice()->EndScene();
	directX->getDevice()->Present(NULL, NULL, NULL, NULL);

}

void checkUserInput(HWND window){
	
	//H�r kontrolleras tangentbordstryck samt mustryck.
	
	input->poll(window);

	if (input->keyPressed(DIK_ESCAPE)){
		
		//ESC anv�nds lite olika, den antingen �ppnar upp menyn, st�nger den eller avslutar spelet
		switch (gameStatus){
			case MAINMENU:
				gameRunning = false;
				break;
			case INGAMEMENU:
				gameStatus = RUNNING;
				break;
			case SPLASHSCREEN:
				gameStatus = MAINMENU;
				break;
			case RUNNING:
			case PAUSED:
			default:
				gameStatus = INGAMEMENU;
				break;
		}
	}
	
	//Ett g�ng tangenter som anv�nds vid splashscreen f�r att ta sig till huvudmenyn
	if (gameStatus == SPLASHSCREEN){

		if (input->keyPressed(DIK_SPACE)){
			gameStatus = MAINMENU;
		}

		if (input->keyPressed(DIK_RETURN)){
			gameStatus = MAINMENU;
		}

		if (input->keyPressed(DIK_NUMPADENTER)){
			gameStatus = MAINMENU;
		}

		if (input->mouseButton(0))
			gameStatus = MAINMENU;

		if (input->mouseButton(1))
			gameStatus = MAINMENU;
	}

	//***************Mushantering vid menyn***************
	
	//Musen anv�nds bara i menyl�get och �r vanlig box-box-kollisionhantering mellan
	//knapparnas sprites och muspekarens. Kollision testas n�r man klickar p� v�nster musknapp.

	if (gameStatus == MAINMENU || gameStatus==INGAMEMENU){

		if (input->mouseButton(0)){
			if (detectBoxCollision(mainMenu->getMouseCursor(), mainMenu->getNewGameButton())){
				gameStatus = LOADSCREEN;
				stage = 1;
			}
		}

		if (input->mouseButton(0)){
			if (detectBoxCollision(mainMenu->getMouseCursor(), mainMenu->getExitGameButton())){
				gameRunning = false;
			}
		}

		if (input->mouseButton(0)){
			if (detectBoxCollision(mainMenu->getMouseCursor(), mainMenu->getSaveButton())){
				
				player->save("Saves//Save1.txt", stage);
				level[0]->save("Saves//Save1.txt");
				MessageBox(NULL, "Saved!", "Load/Save", MB_OK);
			}
		}

		if (input->mouseButton(0)){
			if (detectBoxCollision(mainMenu->getMouseCursor(), mainMenu->getLoadButton())){
				player->load("Saves//Save1.txt");
				stage = player->atLevel();
				gameStatus = LOADSCREEN;
				//MessageBox(NULL, "Finns inget h�r �n, men det �r jag som gjort spelet. Helt sj�lv. Faktiskt.", "Credits", MB_OK);
			}
		}
	}

	if (gameStatus == INGAMEMENU){
		if (input->mouseButton(0)){
			if (detectBoxCollision(mainMenu->getMouseCursor(), mainMenu->getResumeGameButton())){
				gameStatus = RUNNING;
			}
		}
	}

	//Om spelet k�rs s� kontrolleras knappar f�r riktning av spelaren samt space f�r avfyrning av skott
	if (gameStatus == RUNNING){
		if (!player->isExploding()){
			if (input->keyDown(DIK_RIGHTARROW)){
				player->movePlayerX(6.0f);
			}

			if (input->keyDown(DIK_LEFTARROW)){
				player->movePlayerX(-6.0f);
			}

			if (input->keyDown(DIK_UPARROW)){
				player->movePlayerY(-6.0f);
			}

			if (input->keyDown(DIK_DOWNARROW)){
				player->movePlayerY(6.0f);
			}

			if (input->keyDown(DIK_NUMPAD6)){
				player->movePlayerX(6.0f);
			}

			if (input->keyDown(DIK_NUMPAD4)){
				player->movePlayerX(-6.0f);
			}

			if (input->keyDown(DIK_NUMPAD6)){
				player->movePlayerY(-6.0f);
			}

			if (input->keyDown(DIK_NUMPAD2)){
				player->movePlayerY(6.0f);
			}

			if (input->keyDown(DIK_NUMPAD9)){
				player->movePlayerX(6.0f);
				player->movePlayerY(-6.0f);
			}

			if (input->keyDown(DIK_NUMPAD3)){
				player->movePlayerY(6.0f);
				player->movePlayerX(6.0f);
			}

			if (input->keyDown(DIK_NUMPAD7)){
				player->movePlayerY(-6.0f);
				player->movePlayerX(-6.0f);
			}

			if (input->keyDown(DIK_NUMPAD1)){
				player->movePlayerY(+6.0f);
				player->movePlayerX(-6.0f);
			}

			if (input->keyPressed(DIK_SPACE)){
				fireShot();
			}
		}
	}

	//Och s� en pauskontroll med P eller Pauseknappen
	if (gameStatus == RUNNING || gameStatus == PAUSED){
		if (input->keyPressed(DIK_P)){
			if (gameStatus == RUNNING)
				gameStatus = PAUSED;
			else
				gameStatus = RUNNING;
		}

		if (input->keyPressed(DIK_PAUSE)){
			if (gameStatus == RUNNING)
				gameStatus = PAUSED;
			else
				gameStatus = RUNNING;
		}
	}

}

void checkLevelCompleted(){
	
	//Om niv�n �r klar s� v�xlar spelet upp till n�sta niv�
	if (level[0]->isComplete()){
		stage++;
  		player->save("Saves\\Save1.txt", stage);
		gameStatus = LOADSCREEN;
	}
}

void gameLoop(HWND window){
	
	//Spelets huvudloop 

	//Om spelet �r ig�ng s� kontrolleras attacker, r�relser, kollisionshantering
	//om banan �r avklarad etc.
	if (gameStatus == RUNNING || gameStatus==GAMEOVER){
		if (gameStatus != PAUSED){
			enemyAttackV2();
			spawnEnemies();
			moveSprites();
			moveShots();
			checkCollisions();
			player->checkDeath();
			checkLevelCompleted();
			checkPlayerOutOfBounds();
		}
	}
	
	//Vid alla tillf�llen s� ritas grafik ut och anv�ndarens input kontrolleras alltid.
	checkUserInput(window);
	renderGraphics(window);
	calculateMemoryUsage();
	
	//Om laddbilden visas s� l�ses en ny niv� in om spelaren klarat en bana
	if (gameStatus == LOADSCREEN){
		if (stage > 1){
			setUpNewLevel();
		}
		else{
			//alternativt nollst�lls.
			resetGame();
		}
	}

	//Gameover kontrolleras n�r spelet k�rs men �ven n�r spelet �r i GameOverl�ge,
	//det h�r beror p� timern som k�rs n�r spelaren �r d�d.
	if (gameStatus==RUNNING || gameStatus==GAMEOVER)
		checkGameOver();
		

}

//H�r tas alla pekare bort som �r skapade med new.

void deleteGUI(){

	delete splashScreen;
	delete loadScreen;
	delete mainMenu;
	delete hudPanel;

}

void cleanUp(){

	deleteGUI();

	delete player;
	
	for (int i = 0; i < PLAYERSHOTS; i++){
		delete shots[i];
	}

	delete gameOverTimer;
	delete fire;
	delete explosion;

	clearAllLevels();

	delete input;
	delete directX;
}