/*

	En simpel fiende som inte skjuter utan bara f�ljer sina m�lpunkter

*/

#include "JetPlane.h"
#include "Waypoint.h"
#include "DirectX.h"
#include "Sprite.h"

JetPlane::JetPlane(DirectX* directX, float xPos, float yPos, float speed) :Enemy(directX, xPos, yPos, speed)
{

	//Sprite och lista med m�lpunkter skapas upp

	sprite = new Sprite(63, 64, 0, 0, xPos, yPos, 1, 1, 1, directX, "Graphics\\Sprites\\JetPlane.png");

	waypoint.push_back(new Waypoint(427, 25));
	waypoint.push_back(new Waypoint(329, 123));
	waypoint.push_back(new Waypoint(433, 713));
	waypoint.push_back(new Waypoint(913, 472));
	waypoint.push_back(new Waypoint(780, 571));
	waypoint.push_back(new Waypoint(677, 644));
	waypoint.push_back(new Waypoint(-200, 500));
	
	currentWaypoint = 0;

}


JetPlane::~JetPlane()
{
}


