#pragma once

#include <vector>
#include "ISerializable.h"

//Forward declarations

class Background;
class Timer;
class Shot;
class Enemy;
class DirectX;
class Player;

using namespace std;

class Level:ISerializable
{

protected:
	
	DirectX *directX;

	bool levelComplete;
	
	int noOfEnemies;
	int noOfShots;

	int spawnedEnemies;

	vector<Shot*> shotVector;
	vector<Enemy*> enemyVector;
	
	Timer *spawnTimer;
	Background *background;
	Background *clouds;

	Player* player;

	void createBackgroundTileMap();
	
public:
	Level(DirectX *directX, Player *player);
	virtual ~Level();
	
	Shot* getEnemyShots(int pos);
	Enemy* getEnemies(int pos);
	Timer* getSpawnTimer();
	int getENEMYSHOTS(); 
	int getENEMIES(); 
	int getSpawnedEnemies();
	void increaseSpawnedEnemies();
	
	void scrollBackground(bool scroll);
	virtual void updateSpawnTimer() = 0;
	bool isComplete();

	Player* getPlayer();

	virtual void save(string file, int level = -1) override;
	virtual void load(string file) override;

};

