#pragma once

class DirectX;
class Sprite;
class Player;

class HUD
{

private:
	Sprite* mainHUD;
	Sprite* redHealth;
	Sprite* yellowHealth;
	Sprite* greenHealth;
	Sprite* lifeMarker;
	Player* player;
	
	DirectX* directX;



	void destroyHUD();

public:
	HUD(DirectX* directX, Player* player);
	~HUD();

	void drawHUD();
};

