#include "Level2.h"
#include "Background.h"
#include "Timer.h"
#include "Dragon.h"
#include "GameEngine.h"
#include "SkyPirate.h"
#include "QuadShot.h"
#include "HomingMissile.h"

Level2::Level2(DirectX *directX, Player* player) :Level(directX, player)
{

	background = new Background(directX, "Level2", 1.0f, 255);

	for (int i = 0; i < 25; i++){
		
		float dragonStartPos = rand() % ((screenWidht - 150) - 0) + 0;
		enemyVector.push_back(new Dragon(directX, dragonStartPos, -400, 10.0f));

	}

	for (int i = 0; i < 2; i++){
		enemyVector.push_back(new SkyPirate(directX, 900, 1200, 6.0f));
	}

	for (int i = 0; i < 50; i++){
		shotVector.push_back(new QuadShot(directX));
	}

	for (int i = 0; i < 5; i++){
		shotVector.push_back(new HomingMissile(directX));
	}

	noOfEnemies = enemyVector.size();
	noOfShots = shotVector.size();

	spawnTimer->setNewDelay(500);

}

Level2::~Level2()
{
}

void Level2::updateSpawnTimer(){

	if (spawnedEnemies == 25)
		spawnTimer->setNewDelay(5000);

}
