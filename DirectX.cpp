#include <string>
#include "DirectX.h"


using namespace std;

//**************************PRIVATE********************************************

bool DirectX::direct3DInit(HWND window){

	//Gr�nsnittet skapas upp
	direct3DInterface = Direct3DCreate9(D3D_SDK_VERSION);
	if (!direct3DInterface)
		return false;

	//setMaxResolution(); <--�NDRA DEN H�R SEDAN N�R SKALNINGEN �R F�RDIG
	if (!setParameters(window, screenWidht, screenHeight))
		return false;

	return true;
}


bool DirectX::setParameters(HWND window, int width, int height){
	D3DPRESENT_PARAMETERS d3dPresentParameters;
	ZeroMemory(&d3dPresentParameters, sizeof(d3dPresentParameters)); //<-Nollar parametrar
	d3dPresentParameters.Windowed = true; //<-F�nsterl�ge
	d3dPresentParameters.SwapEffect = D3DSWAPEFFECT_COPY; //<-Hur backbuffern ska bytas ut
	d3dPresentParameters.BackBufferFormat = D3DFMT_X8R8G8B8; //<-Backbufferns format (�r os�ker p� den h�r, men f�r lita p� att kurslitteraturen har r�tt!)
	d3dPresentParameters.BackBufferCount = 1; //<-Antal backbuffrar
	d3dPresentParameters.BackBufferHeight = height; //<-h�jd...
	d3dPresentParameters.BackBufferWidth = width; //<--...och bredd p� backbuffern

	HRESULT result = direct3DInterface->CreateDevice(D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, window, D3DCREATE_SOFTWARE_VERTEXPROCESSING, &d3dPresentParameters, &direct3DDevice);
	if (result != D3D_OK)
		return false;

	result = direct3DDevice->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &surfaceBackbuffer);
	if (result != D3D_OK)
		return false;

	if (!direct3DDevice)
		return false;

	if (!direct3DDevice)
		return false;

	initFonts();

	return true;
}

void DirectX::setMaxResolution(){
	//R�knar ut storleken p� sk�rmen
	RECT desktopResolution;
	const HWND window = GetDesktopWindow();
	GetWindowRect(window, &desktopResolution);
	screenHeight = desktopResolution.bottom;
	screenWidht = desktopResolution.right;
}

void DirectX::initFonts(){

	HDC hdc = GetDC(0); // Sk�rmens DC
	int textSize = -MulDiv(35, GetDeviceCaps(hdc, LOGPIXELSY), 72); //<-R�knar om pixlar till logiska enheter f�r fontstorleken

	D3DXFONT_DESC fontParameters = { textSize, 0, 0, 0, false, DEFAULT_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_PITCH, "Calibri" };
	D3DXCreateFontIndirect(direct3DDevice, &fontParameters, &defaultFont);

}

//**************************PUBLIC********************************************

DirectX::DirectX(HWND window){
	
	if (!direct3DInit(window))
		return;
	transparancy = true;
}

DirectX::~DirectX(){

}

bool DirectX::resetResoution(HWND window, int width, int height){
	//St�ller in DirectX-parametrar

	shutDown(); //Nollar interface, enhet, fonter etc

	if (!setParameters(window, width, height)) //skapar om DirectX-enheten med ny storlek
		return false;

	screenHeight = height;
	screenWidht = width;

	return true;

}

LPDIRECT3DDEVICE9 DirectX::getDevice(){
	//Returnerar directX-enheten
	return direct3DDevice;
}

void DirectX::clearScreen(D3DCOLOR color){
	//Fyller sk�rmen i en f�rg
	if (direct3DDevice)
		direct3DDevice->Clear(0, NULL, NULL, D3DCOLOR(color), 0, 0);

}

void DirectX::shutDown(){
	//Avallokerar minne och st�nger ner DX-funktionerna s� att de inte ligger och k�r efter programavslut

	if (direct3DInterface){
		if (direct3DDevice){
			if (defaultFont)
				defaultFont->Release();
			direct3DDevice->Release();
		}
		direct3DInterface->Release();
	}
}

void DirectX::printText(const char* text, float x, float y, D3DCOLOR fontColor){

	RECT fontArea = { (long)x, (long)y, 0, 0 };
	defaultFont->DrawTextA(NULL, text, -1, &fontArea, DT_NOCLIP, fontColor);
}

void DirectX::printText(const char* text, string font, int size, float x, float y){

	//En �verladdad version av ovanst�ende metod, mest f�r min egen skull. Eventuellt bygger jag ut den h�r till andra projekt. 
	//Den h�r skulle t ex kunna ta emot f�rg, kursivt etc

	HDC hdc = GetDC(0); // Sk�rmens DC
	int textSize = -MulDiv(size, GetDeviceCaps(hdc, LOGPIXELSY), 72); //<-R�knar om pixlar till logiska enheter f�r fontstorleken
	LPD3DXFONT customFont;

	D3DXFONT_DESC fontParameters = { textSize, 0, 0, 0, false, DEFAULT_CHARSET, OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_PITCH, NULL };
	strcpy(fontParameters.FaceName, font.c_str());
	D3DXCreateFontIndirect(direct3DDevice, &fontParameters, &customFont);

	RECT fontArea = { (long)x, (long)y, 0, 0 };
	customFont->DrawTextA(NULL, text, -1, &fontArea, DT_NOCLIP, D3DCOLOR_RGBA(238, 20, 20, 255));
	

	customFont->Release();
	delete customFont;

}

LPDIRECT3DTEXTURE9 DirectX::loadTexture(string filename, D3DCOLOR transcolor){

	//Ladda in bild till texture f�r sprites med hj�lp av DirectX:s egna funktioner
	//Den tar emot filnamn och vilken f�rg som ska vara transparent

	LPDIRECT3DTEXTURE9 texture = NULL;

	D3DXIMAGE_INFO info;
	HRESULT result = D3DXGetImageInfoFromFile(filename.c_str(), &info);
	if (result != D3D_OK)
		return NULL;

	//Sedan skapas en textur som kan anv�ndas f�r sprites

	D3DXCreateTextureFromFileEx(direct3DDevice, filename.c_str(), info.Width, info.Height, 1, D3DPOOL_DEFAULT, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, transcolor, &info, NULL, &texture);

	if (result != D3D_OK)
		return NULL;

	return texture;
}

void DirectX::setTransparancy(){
	//S�tter p� eller st�nger av transparensen

	if (!transparancy)
		transparancy = true;
	else
		transparancy = false;
}

bool DirectX::getTransparancy(){
	//Returnerar om transparensen �r p� eller av

	return transparancy;
}

LPDIRECT3DSURFACE9 DirectX::loadSurface(string filename){

	//Tar in en bild och l�ser in den i en rityta

	LPDIRECT3DSURFACE9 image = NULL;


	//Tar fram information om filen(s� som h�jd och bredd)
	D3DXIMAGE_INFO info;
	HRESULT result = D3DXGetImageInfoFromFile(filename.c_str(), &info);
	if (result != D3D_OK)
		return NULL;

	//G�r en surface i samma storlek som bilden
	result = direct3DDevice->CreateOffscreenPlainSurface(info.Width, info.Height, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, &image, NULL);
	if (result != D3D_OK)
		return NULL;

	//L�ser in bilden i ritytan och returnerar den
	result = D3DXLoadSurfaceFromFile(image, NULL, NULL, filename.c_str(), NULL, D3DX_DEFAULT, D3DCOLOR_XRGB(0, 0, 0), NULL);
	if (result != D3D_OK)
		return NULL;

	return image;

}

LPDIRECT3DSURFACE9 DirectX::getBackbuffer(){
	return surfaceBackbuffer;
}