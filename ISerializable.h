#pragma once

#include <string>
#include <fstream>

using namespace std;

class ISerializable
{
public:
	ISerializable();
	virtual ~ISerializable();

	virtual void load(string file)=0;	
	virtual void save(string file, int level=-1)=0;


};

