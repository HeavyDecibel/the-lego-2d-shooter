/*
Basklass f�r niv� som agerar som abstrakt klass. Den kommer aldrig att impementeras sj�lv.
Klassen har i uppgift att h�lla koll p� fiender och skott, dels hur m�nga de �r och dels vilken status de har.
Den har �ven i uppgift att sk�ta om bakgrunderna. Det finns tv� stycken, en "huvudbakgrund" som l�ses in av respektive niv�
samt en molnbakgrund (f�r att f� till en parallaxeffekt). 
*/

#include "Level.h"
#include "Background.h"
#include "Timer.h"
#include "Enemy.h"
#include "Shot.h"
#include "Player.h"
#include "Waypoint.h"

Level::Level(DirectX *directX, Player *player):
directX(directX),
player(player)
{
	
	//Konstruktor som tar referenser av directX och player f�r ber�kningar
	//H�r finns �ven en timer som styr n�r de olika fienderna dyker upp
	//Samt tv� bakgrunder

	spawnTimer = new Timer(5000);
	spawnedEnemies = 0;
	levelComplete = false;
	createBackgroundTileMap();
	clouds = new Background(directX, "Clouds", 5.0f, 150);

}


Level::~Level()
{

	//Destruktor som rensar vektorer med fiender och skott
	//samt timer och de tv� bakgrunderna

	for (int i = 0; i < enemyVector.size(); i++)
		delete enemyVector[i];

	enemyVector.clear();
	enemyVector.shrink_to_fit();

	for (int i = 0; i < shotVector.size(); i++)
		delete shotVector[i];

	shotVector.clear();
	shotVector.shrink_to_fit();

	delete spawnTimer;
	delete background;
	delete clouds;
}

Shot* Level::getEnemyShots(int pos){

	//Returnerar ett unikt skott i skottvektorn
	return shotVector[pos];
}

Enemy* Level::getEnemies(int pos) {

	//Returnerar en unik fiende
	return enemyVector[pos];
}

int Level::getENEMYSHOTS(){

	//Returnerar skott totalt
	return noOfShots;
}

int Level::getENEMIES(){

	//Returnerar antalet fiender totalt
	return noOfEnemies;
}

Timer* Level::getSpawnTimer(){
	//Returnerar timern
	return spawnTimer;
}

void Level ::scrollBackground(bool scroll){

	//Skrollar bakgrunden om satt till true (det h�r f�r att kunna stoppa scrollning vid paus etc)
	if (scroll){
		background->scrollBackground();
		clouds->scrollBackground();
	}
		
	background->drawBackground();
	clouds->drawBackground();
	
}

int Level::getSpawnedEnemies(){
	//Returnerar antalet fiender som �r utskickade p� sk�rmen
	return spawnedEnemies;
}

void Level::increaseSpawnedEnemies(){
	//�kar antalet utskickade fiender
	spawnedEnemies++;
}


Player* Level::getPlayer(){
	//Returnerar spelaren (eller skickar vidare den snarare)
	return player;
}

bool Level::isComplete(){
	
	//Om samtliga fiender i fiendevectorn �r spawnade och inaktiva
	//dvs de �r antingen d�da eller har �kt utanf�r sk�rmen
	//s� �r banan avklarad

	int inactiveEnemies = 0;

	for (int i = 0; i < enemyVector.size(); i++){
		if (!enemyVector[i]->isActive())
			inactiveEnemies++;
	}

	if (spawnedEnemies == enemyVector.size() && inactiveEnemies == enemyVector.size())
		levelComplete = true;
	
	return levelComplete;
}

void Level::createBackgroundTileMap(){

	//Skapar upp en tilemap-fil med slumpm�ssiga moln
	//genom att g�ra 36*16 rader med sifforna 0-6

	ofstream tileMapFile;
	tileMapFile.open("Levels//clouds.txt");

	int cloudType;
	string textRow="";

	for (int rows = 0; rows < 36; rows++){
		for (int cols = 0; cols < 17; cols++){
			if (cols == 16){
				textRow += "\n";
			}
			else{
				cloudType = rand() % 8;
				textRow += to_string(cloudType);
			}
		}
		tileMapFile << textRow.c_str();
		textRow = "";
	}

	tileMapFile.close();

}

#pragma region Serialisation

void Level::load(string file){

	//En metod som var t�nkt av deserialisera level... Det f�r bli till en annan g�ng
	background->load(file);

}

void Level::save(string file, int level){

	//En stubbe av en metod som ska serialisera alla niv�r. Den sparar ner fiender, skott etc. Men det �r f�r mycket 
	//jobb med att hinna f� med allt s� jag l�ter den vara kvar som ett tecken p� att jag f�rst�r principen i alla fall ;-)
	background->save(file);

	ofstream saveFile;
	saveFile.open(file, ios::app);

	string levelSaveRow = "";
	string enemyDataRow = "";

	levelSaveRow += to_string(level) + ",";
	levelSaveRow += to_string(noOfEnemies) + ",";
	levelSaveRow += to_string(noOfShots) + ",";
	levelSaveRow += to_string(spawnedEnemies) + ",";
	levelSaveRow += to_string(spawnTimer->getTime()) + "\n";

	saveFile << levelSaveRow.c_str();

	for (int i = 0; i < noOfEnemies; i++){
		enemyDataRow = "";
		enemyDataRow += to_string(enemyVector[i]->getX()) + ",";
		enemyDataRow += to_string(enemyVector[i]->getY()) + ",";
		enemyDataRow += to_string(enemyVector[i]->getStrenght()) + ",";
		
		if (enemyVector[i]->isActive())
			enemyDataRow +="1,";
		else
			enemyDataRow += "0,";

		if (enemyVector[i]->isDestroyed())
			enemyDataRow += "1,";
		else
			enemyDataRow += "0,";


		if (enemyVector[i]->getExploded())
			enemyDataRow += "1\n";
		else
			enemyDataRow += "0\n";

		saveFile << enemyDataRow.c_str();

	}

	saveFile.close();

}

#pragma endregion