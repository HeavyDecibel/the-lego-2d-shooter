#include "HUD.h"
#include "DirectX.h"
#include "Sprite.h"
#include "Player.h"


HUD::HUD(DirectX* directX, Player* player):
directX(directX),
player(player)
{

	mainHUD = new Sprite(1920, 360, 0, 0, 1, 969, 1, 1, 1, directX, "Graphics/GUI/HUDBottom.png");
	greenHealth = new Sprite(50, 100, 0, 0, 525, 975, 1, 1, 1, directX, "Graphics/GUI/GreenHealth.png");
	yellowHealth = new Sprite(50, 100, 0, 0, 625, 975, 1, 1, 1, directX, "Graphics/GUI/YellowHealth.png");
	redHealth = new Sprite(50, 100, 0, 0, 725, 975, 1, 1, 1, directX, "Graphics/GUI/RedHealth.png");
	lifeMarker = new Sprite(100, 100, 0, 0, 1100, 975, 1, 1, 1, directX, "Graphics/GUI/LifeMarker.png");

}


HUD::~HUD()
{

	destroyHUD();

}

void HUD::destroyHUD(){

	delete mainHUD;
	delete greenHealth;
	delete yellowHealth;
	delete redHealth;
	delete lifeMarker;

}

void HUD::drawHUD(){

	mainHUD->drawSprite();
	
	if (player->getHealth() > 5){
		greenHealth->setNewPos(525, 975);
		greenHealth->drawSprite();
	}

	if (player->getHealth() > 4){
		greenHealth->setNewPos(575, 975);
		greenHealth->drawSprite();
	}
	
	if (player->getHealth() > 3){
		yellowHealth->setNewPos(625, 975);
		yellowHealth->drawSprite();
	}

	if (player->getHealth() > 2){
		yellowHealth->setNewPos(675, 975);
		yellowHealth->drawSprite();
	}

	if (player->getHealth() > 1){
		redHealth->setNewPos(725, 975);
		redHealth->drawSprite();
	}

	if (player->getHealth() > 0){
		redHealth->setNewPos(775, 975);
		redHealth->drawSprite();
	}

	for (int i = 0; i < player->getLives(); i++){
		
		lifeMarker->setNewPos(1100+(101*i), 975);
		lifeMarker->drawSprite();
		
	}

}
