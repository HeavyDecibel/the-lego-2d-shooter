/*

	En bossfiende som har en st�rre explosionssprite och tv� anfall. Dels m�ls�kande missiler men �ven quadshots som sekund�rattack

*/


#include "SkyPirate.h"
#include "Level.h"
#include "Shot.h"
#include "HomingMissile.h"
#include "QuadShot.h"
#include "Sprite.h"
#include "Waypoint.h"
#include "Timer.h"
#include "Player.h"


SkyPirate::SkyPirate(DirectX* directX, float xPos, float yPos, float speed) :Enemy(directX, xPos, yPos, speed)
{

	//Sprites, waypoints etc skapas upp. Men �ven 50 i styrka s� att den blir lite tuffare att d�da �n dom andra

	sprite = new Sprite(240, 181, 0, 0, xPos, yPos, 1, 1, 1, directX, "Graphics/Sprites/SkyPirate.png");

	delete explosionSprite;
	explosionSprite = new Sprite(256, 256, 0, 0, 0, 0, 16, 4, 4, directX, "graphics/Explosions/BigExplosion.png");

	waypoint.push_back(new Waypoint(900, 700));
	
	waypoint.push_back(new Waypoint(300, 700));
	waypoint.push_back(new Waypoint(200, 600));
	
	waypoint.push_back(new Waypoint(200, 200));
	waypoint.push_back(new Waypoint(300, 100));
	
	waypoint.push_back(new Waypoint(1500, 100));
	waypoint.push_back(new Waypoint(1600, 200));
	
	waypoint.push_back(new Waypoint(1600, 600));
	waypoint.push_back(new Waypoint(1500, 700));

	strenght = 50;

	secondaryAttackTimer = new Timer(250); //Och en sekund�rattacktimer.
	attackTimer->setNewDelay(5000);

}


SkyPirate::~SkyPirate()
{
	delete secondaryAttackTimer;
	
}

void SkyPirate::move(){

	//Den h�r fienden anv�nder samma r�relsefunktion som basklassen
	//men n�r den n�r sin slutpunkt s� b�rjar den om igen, det h�r f�r att den
	//ska kunna cirkulera runt p� spelsk�rmen. D�rf�r kr�vs ett extra till�gg som 
	//nollar vilka waypoints som har varit bes�kta.
	Enemy::move();
	if (lastWaypointReached){
		active = true;
		lastWaypointReached = false;
		currentWaypoint = 0;
		for (int i = 0; i < waypoint.size(); i++){
			waypoint[i]->reached = false;
		}
	}
}

void SkyPirate::attackV3(Level *level){


	//Den h�r fienden har tv� attacker

	//F�rst m�ls�kande missil:

	//S�tter ett nytt m�l f�r missilen s� den alltid r�r sig mot spelaren
	for (int i = 0; i < level->getENEMYSHOTS(); i++){
		if (level->getEnemyShots(i)->isActive() && level->getEnemyShots(i)->getShotType() == HOMINGMISSILE){
			level->getEnemyShots(i)->setTarget(level->getPlayer());
			if (level->getPlayer()->isExploding())
				level->getEnemyShots(i)->toggleStatus();
		}
	}
	
	//Letar upp l�gsta inaktiva missilnumret
	if (attackTimer->done()){
		if (!level->getPlayer()->isExploding()){
			for (int k = 0; k < level->getENEMYSHOTS(); k++){ 
				if (!level->getEnemyShots(k)->isActive() && level->getEnemyShots(k)->getShotType() == HOMINGMISSILE){
					level->getEnemyShots(k)->toggleStatus();
					level->getEnemyShots(k)->getSprite()->setNewPos(this->getSprite()->getX(), this->getSprite()->getY());
					level->getEnemyShots(k)->setTarget(level->getPlayer());
					break;
				}
			}
		}
	}


	//Och sedan fyra quadshots
	if (secondaryAttackTimer->done()){
		for (int j = 0; j < 4; j++){
			for (int k = 0; k < level->getENEMYSHOTS(); k++){ //Letar upp l�gsta inaktiva missilnumret
				if (!level->getEnemyShots(k)->isActive() && level->getEnemyShots(k)->getShotType() == QUADSHOT){
					level->getEnemyShots(k)->toggleStatus();
					level->getEnemyShots(k)->getSprite()->setNewPos(this->getSprite()->getX(), this->getSprite()->getY());
					break;
				}
			}
		}
	}

}